package com.jssoftcom.comanda

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.provider.Settings.Secure
import com.jssoftcom.comanda.database.AppDataBase
import com.jssoftcom.comanda.settings.parameters.ParametersPreference


/**
 * Created by gustavon on 27/10/17.
 */
open class ComandaApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        ComandaApplication.database =  Room.databaseBuilder(this, AppDataBase::class.java, "comanda_android.db").build()
        ComandaApplication.nomeDispositivo =  ParametersPreference.instance.getNameDevice(this, ParametersPreference.NAME_DEVICE)
        ComandaApplication.vendorId =  Secure.getString(getContentResolver(), Secure.ANDROID_ID);
    }

    public override fun getApplicationContext(): Context {
        return super.getApplicationContext()
    }

    companion object {
        lateinit var database: AppDataBase
        lateinit var nomeDispositivo: String
        lateinit var vendorId: String
    }

}
