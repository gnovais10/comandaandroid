package com.jssoftcom.comanda.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.home.HomeActivity

class SplashScreenActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val handler = Handler()
        handler.postDelayed(Runnable {
            startActivity(Intent(this, HomeActivity::class.java))
        }, 3000)
    }
}
