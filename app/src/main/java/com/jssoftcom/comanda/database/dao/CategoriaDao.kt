package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.database.tables.Produto
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import java.nio.channels.FileLock

/**
 * Created by gustavon on 02/11/17.
 */
@Dao
interface CategoriaDao {

    @Query("SELECT * FROM Categorias ORDER BY Ordem ASC")
    fun getCategorias(): Flowable<List<Categorias>>

    @Query("SELECT * FROM Categorias ORDER BY Descricao DESC")
    fun getCategoriaOrderByDes(): Flowable<List<Categorias>>

    @Insert
    fun insert(categorias: Categorias)

    @Query("DELETE FROM Categorias")
    fun deleteAll()
}