package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.database.tables.Combinados
import com.jssoftcom.comanda.database.tables.Opcoes
import io.reactivex.Flowable

/**
 * Created by gustavon on 04/11/17.
 */
@Dao
interface CombinadoDao {

    @Query("SELECT * FROM Combinados")
    fun getOpcoes(): Flowable<List<Opcoes>>

    @Insert
    fun insert(combinados: Combinados)

    @Query("DELETE FROM Combinados")
    fun deleteAll()
}