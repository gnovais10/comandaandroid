package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.database.tables.Opcoes
import io.reactivex.Flowable

/**
 * Created by gustavon on 04/11/17.
 */
@Dao
interface OpcoesDao {

    @Query("SELECT * FROM Opcoes")
    fun getOpcoes(): Flowable<List<Opcoes>>

    @Insert
    fun insert(opcoes: Opcoes)

    @Query("DELETE FROM Opcoes")
    fun deleteAll()

    @Query("SELECT * FROM Opcoes WHERE IDCategoria = :arg0 ORDER BY Ordem")
    fun getOpcoesByCategoria(idCategoria: String): Flowable<List<Opcoes>>

    @Query("SELECT * FROM Opcoes WHERE IDOpcao = :arg0 ORDER BY Ordem")
    fun getOpcoesById(id: String): Flowable<Opcoes>
}