package com.jssoftcom.comanda.database.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by gustavon on 02/11/17.
 */
@Entity(tableName = "Combinados")
class Combinados {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "IDProduto")
    lateinit var IDProduto: String

    @ColumnInfo(name = "IDProdutoReferente")
    lateinit var IDProdutoReferente: String
}