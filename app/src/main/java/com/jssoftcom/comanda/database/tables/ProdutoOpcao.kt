package com.jssoftcom.comanda.database.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by gustavon on 02/11/17.
 */
@Entity(tableName = "ProdutoOpcao")
class ProdutoOpcao {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "IDProdutoOpcao")
    lateinit var IDProdutoOpcao: String

    @ColumnInfo(name = "IDProduto")
    lateinit var IDProduto: String

    @ColumnInfo(name = "Descricao")
    lateinit var Descricao: String

    @ColumnInfo(name = "Ordem")
    lateinit var Ordem: String
}
