package com.jssoftcom.comanda.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.jssoftcom.comanda.database.dao.*
import com.jssoftcom.comanda.database.tables.*

/**
 * Created by gustavon on 02/11/17.
 */
@Database(entities = arrayOf(Categorias::class, Combinados::class, Opcoes::class, ProdutoOpcao::class, Produto::class, SubCategorias::class), version = 10)
abstract class AppDataBase : RoomDatabase() {
    abstract fun produtosDao(): ProdutoDao
    abstract fun categoriasDao(): CategoriaDao
    abstract fun subCategoriasDao(): SubCategoriaDao
    abstract fun opcoesDao(): OpcoesDao
    abstract fun combinadosDao(): CombinadoDao
    abstract fun produtoOpcao(): ProdutoOpcaoDao
}