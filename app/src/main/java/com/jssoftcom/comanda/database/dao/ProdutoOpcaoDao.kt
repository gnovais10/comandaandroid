package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.database.tables.ProdutoOpcao
import io.reactivex.Flowable

/**
 * Created by gustavon on 02/11/17.
 */
@Dao
interface ProdutoOpcaoDao {

    @Query("SELECT * FROM ProdutoOpcao")
    fun getProdutoOpcao(): Flowable<List<ProdutoOpcao>>

    @Insert
    fun insert(produtoOpcao: ProdutoOpcao)

    @Query("DELETE FROM ProdutoOpcao")
    fun deleteAll()
}