package com.jssoftcom.comanda.database.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by gustavon on 02/11/17.
 */

@Entity(tableName = "Produto")
class Produto {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "IDProduto")
    lateinit var IDProduto: String

    @ColumnInfo(name = "Descricao")
    lateinit var Descricao: String

    @ColumnInfo(name = "Preco")
    lateinit var Preco: String

    @ColumnInfo(name = "Combinado")
    lateinit var Combinado: String

    @ColumnInfo(name = "IDCategoria")
    lateinit var IDCategoria: String

    @ColumnInfo(name = "IDSubCategoria")
    lateinit var IDSubCategoria: String

    @ColumnInfo(name = "SubCategoria")
    lateinit var SubCategoria: String

    @ColumnInfo(name = "Ordem")
    lateinit var Ordem: String

    @ColumnInfo(name = "MultiploVenda")
    lateinit var MultiploVenda: String

    @ColumnInfo(name = "DigitaQuantidade")
    lateinit var DigitaQuantidade: String

}
