package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.SubCategorias
import io.reactivex.Flowable


/**
 * Created by gustavon on 02/11/17.
 */
@Dao
interface SubCategoriaDao {

    @Query("SELECT * FROM SubCategorias")
    fun getAllSubCategorias(): Flowable<List<SubCategorias>>


    /*/
    "SELECT * FROM book "
           + "INNER JOIN loan ON loan.book_id = book.id "
           + "INNER JOIN user ON user.id = loan.user_id "
           + "WHERE user.name LIKE :userName"
     */

    @Query("SELECT DISTINCT SB.IDSubcategoria, SB.Descricao, SB.Ordem\n" +
            "FROM SubCategorias SB\n" +
            "inner join Produto P on P.IDSubcategoria = SB.IDSubCategoria\n" +
            "WHERE P.IDCategoria = :arg0 ORDER BY SB.Descricao DESC")
    fun getSubCategoriaByCategoria(idCategoria: String): Flowable<List<SubCategorias>>

    @Insert
    fun insert(subCategorias: SubCategorias)

    @Query("DELETE FROM SubCategorias")
    fun deleteAll()
}