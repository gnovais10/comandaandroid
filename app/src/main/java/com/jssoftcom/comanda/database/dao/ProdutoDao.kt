package com.jssoftcom.comanda.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.jssoftcom.comanda.database.tables.Opcoes
import com.jssoftcom.comanda.database.tables.Produto
import io.reactivex.Flowable
import io.reactivex.Maybe

/**
 * Created by gustavon on 02/11/17.
 */
@Dao
interface ProdutoDao {

    @Query("SELECT * FROM Produto")
    fun getProdutos(): Flowable<List<Produto>>

    @Insert
    fun insert(produto: Produto)

    @Query("DELETE FROM Produto")
    fun deleteAll()

    @Query("SELECT * FROM Produto WHERE IDSubCategoria = :arg0 and IDCategoria = :arg1")
    fun getProdutosBySubCategoria(idSubCategoria: String, idCategoria : String): Flowable<List<Produto>>
}