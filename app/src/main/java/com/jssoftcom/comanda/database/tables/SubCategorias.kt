package com.jssoftcom.comanda.database.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by gustavon on 02/11/17.
 */
@Entity(tableName = "SubCategorias")
class SubCategorias {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "IDSubCategoria")
    lateinit var IDSubCategoria: String

    @ColumnInfo(name = "Descricao")
    lateinit var Descricao: String

    @ColumnInfo(name = "Ordem")
    lateinit var Ordem: String
}
