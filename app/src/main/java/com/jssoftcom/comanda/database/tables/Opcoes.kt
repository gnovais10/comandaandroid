package com.jssoftcom.comanda.database.tables

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by gustavon on 02/11/17.
 */
@Entity(tableName = "Opcoes")
class Opcoes {

    /*(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, IDOpcao TEXT, IDCategoria TEXT, Descricao TEXT, Ordem INTEGER)*/

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "IDOpcao")
    lateinit var IDOpcao: String

    @ColumnInfo(name = "IDCategoria")
    lateinit var IDCategoria: String

    @ColumnInfo(name = "Ordem")
    lateinit var Ordem: String

    @ColumnInfo(name = "Descricao")
    lateinit var Descricao: String
}

