package com.jssoftcom.comanda.settings.produtos

import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosListenersContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse
import retrofit2.Response

/**
 * Created by gustavon on 02/11/17.
 */
interface ProdutosContract{

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun start()
        fun produtos(produtosListenersContract: ProdutosListenersContract)
        fun insertProdutos(produtosResponse: Response<List<ProdutosResponse>>)
        fun deleteAllProdutos()
    }

}