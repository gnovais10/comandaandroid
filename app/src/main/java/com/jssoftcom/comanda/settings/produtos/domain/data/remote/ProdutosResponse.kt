package com.jssoftcom.comanda.settings.produtos.domain.data.remote

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by gustavon on 02/11/17.
 */
open class ProdutosResponse {

    @SerializedName("Produtos")
    var produtos = ArrayList<ProdutosResponse.Produtos>()

    open class Produtos() : Parcelable {
        @SerializedName("IDProduto")
        lateinit var idProduto: String
        @SerializedName("Descricao")
        lateinit var descricao: String
        @SerializedName("Preco")
        lateinit var preco: String
        @SerializedName("Combinado")
        lateinit var combinado: String
        @SerializedName("IDCategoria")
        lateinit var idCategoria: String
        @SerializedName("IDSubCategoria")
        lateinit var idSubCategoria: String
        @SerializedName("SubCategoria")
        lateinit var subCategoria: String
        @SerializedName("MultiploVenda")
        lateinit var multiploVenda: String
        @SerializedName("Ordem")
        lateinit var ordem: String
        @SerializedName("DigitaQuantidade")
        lateinit var digitaQuantidade: String

        constructor(parcel: Parcel) : this() {
            idProduto = parcel.readString()
            descricao = parcel.readString()
            preco = parcel.readString()
            combinado = parcel.readString()
            idCategoria = parcel.readString()
            idSubCategoria = parcel.readString()
            subCategoria = parcel.readString()
            ordem = parcel.readString()
            multiploVenda = parcel.readString()
            digitaQuantidade = parcel.readString()
        }


        override fun describeContents(): Int {
            return 0
        }

        override fun writeToParcel(parcel: Parcel?, p1: Int) {
            parcel?.writeString(idProduto)
            parcel?.writeString(descricao)
            parcel?.writeString(preco)
            parcel?.writeString(combinado)
            parcel?.writeString(idCategoria)
            parcel?.writeString(idSubCategoria)
            parcel?.writeString(subCategoria)
            parcel?.writeString(ordem)
            parcel?.writeString(multiploVenda)
            parcel?.writeString(digitaQuantidade)
        }

        companion object CREATOR : Parcelable.Creator<Produtos> {
            override fun createFromParcel(parcel: Parcel): Produtos {
                return Produtos(parcel)
            }

            override fun newArray(size: Int): Array<Produtos?> {
                return arrayOfNulls(size)
            }
        }
    }


}