package com.jssoftcom.comanda.settings

import android.content.Context
import com.example.jssoftcom.comanda.infra.ComandaAndroidAPI
import com.jssoftcom.comanda.infra.RetrofitProvider
import com.jssoftcom.comanda.infra.handleError.MyCallBack
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import com.jssoftcom.comanda.settings.domain.data.remote.SettingsListenerContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosListenersContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse
import retrofit2.Response
import java.io.IOException

/**
 * Created by gustavon on 02/11/17.
 */
open class SettingsRemoteRepository(var context: Context) {

    fun produtos(listener: ProdutosListenersContract) {
        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        remoteRepository.produtos().enqueue(object : MyCallBack<List<ProdutosResponse>> {
            override fun success(response: Response<List<ProdutosResponse>>) {
                listener.success(response)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.failure(t)
            }
        })

    }


    fun categorias(listener: SettingsListenerContract) {
        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        remoteRepository.categorias().enqueue(object : MyCallBack<List<SettingsResponse>> {
            override fun success(response: Response<List<SettingsResponse>>) {
                listener.successSettings(response)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }
}