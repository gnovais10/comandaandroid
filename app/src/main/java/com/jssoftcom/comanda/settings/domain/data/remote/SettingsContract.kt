package com.jssoftcom.comanda.settings.domain.data.remote

import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import retrofit2.Response


/**
 * Created by gustavon on 03/11/17.
 */

interface SettingsContract{

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun categorias(settingsListenerContract: SettingsListenerContract)
        fun start()
        fun insertCategorias(settingsResponse: Response<List<SettingsResponse>>)
        fun insertSubCategorias(settingsResponse: Response<List<SettingsResponse>>)
        fun insertOpcao(settingsResponse: Response<List<SettingsResponse>>)
        fun insertCombinados(settingsResponse: Response<List<SettingsResponse>>)
        fun insertProdutoOpcao(settingsResponse: Response<List<SettingsResponse>>)
        fun deleteAll()
    }

}