package com.jssoftcom.comanda.settings.domain.data.remote

import android.content.Context
import android.util.Log
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.database.tables.*
import com.jssoftcom.comanda.settings.SettingsRemoteRepository
import com.jssoftcom.comanda.settings.SettingsRepository
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import io.reactivex.Observable
import retrofit2.Response

/**
 * Created by gustavon on 03/11/17.
 */
class SettingsPresenter(var context: Context) : SettingsContract.Presenter {

    var parametersRepository: SettingsRepository? = null

    override fun start() {
        parametersRepository = SettingsRepository(SettingsRemoteRepository(context))
    }

    override fun categorias(SettingsListenersContract: SettingsListenerContract) {
        parametersRepository!!.categorias(SettingsListenersContract)
    }


    override fun deleteAll() {
        apply {
            ComandaApplication.database.categoriasDao().deleteAll()
            ComandaApplication.database.subCategoriasDao().deleteAll()
            ComandaApplication.database.opcoesDao().deleteAll()
            ComandaApplication.database.combinadosDao().deleteAll()
            ComandaApplication.database.produtosDao().deleteAll()
        }

    }


    override fun insertCategorias(settingsResponse: Response<List<SettingsResponse>>) {
        //insert categorias
        val listObservable = Observable.fromArray(settingsResponse.body()[0].Categorias)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var categoria = Categorias()

                categoria.IDCategoria = value.idCategoria
                categoria.CategoriaPizza = value.categoriaPizza
                categoria.Descricao = value.descricao.toLowerCase()
                categoria.NomeImagem = value.imagem
                categoria.Ordem = value.ordem

                apply {
                    ComandaApplication.database.categoriasDao().insert(categoria)
                }

                Log.i("ROOM_SQLITE","INSERT CATEGORIA" + categoria.IDCategoria)
            } catch (e: Exception) {
                Log.i("ROOM_SQLITE","ERRO INSERT CATEGORIA" + e.printStackTrace())
            }
        }
    }

    override fun insertSubCategorias(settingsResponse: Response<List<SettingsResponse>>) {
        //insert subcategorias
        val listObservable = Observable.fromArray(settingsResponse.body()[1].SubCategorias)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var subCategoria = SubCategorias()

                subCategoria.Descricao = value.descricao.toLowerCase();
                subCategoria.IDSubCategoria = value.idSubCategoria
                subCategoria.Ordem = value.ordem

                apply {
                    ComandaApplication.database.subCategoriasDao().insert(subCategoria)
                }

                Log.i("ROOM_SQLITE","INSERT SUB_CATEGORIA" + subCategoria.IDSubCategoria)
            } catch (e: Exception) {
                Log.i("ROOM_SQLITE","ERRO INSERT SUB_CATEGORIA" + e.printStackTrace())
            }
        }
    }

    override fun insertOpcao(settingsResponse: Response<List<SettingsResponse>>) {
        //insert opcoes
        val listObservable = Observable.fromArray(settingsResponse.body()[2].Opcao)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var opcoes = Opcoes()
                opcoes.IDOpcao = value.idOpcao
                opcoes.IDCategoria = value.idCategoria.toLowerCase()
                opcoes.Ordem = value.ordem
                opcoes.Descricao = value.descricao

                apply {
                    ComandaApplication.database.opcoesDao().insert(opcoes)
                }

                Log.i("ROOM_SQLITE","INSERT OPCAO" + opcoes.IDOpcao)
            } catch (e: Exception) {
                Log.i("ROOM_SQLITE","ERRO INSERT OPCAO" + e.printStackTrace())
            }
        }
    }

    override fun insertCombinados(settingsResponse: Response<List<SettingsResponse>>) {
        //insert combinados
        val listObservable = Observable.fromArray(settingsResponse.body()[3].Combinados)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var combinados = Combinados()
                combinados.IDProduto = value.idProduto
                combinados.IDProdutoReferente = value.idProdutoReferente

                apply {
                    ComandaApplication.database.combinadosDao().insert(combinados)
                }

                Log.i("ROOM_SQLITE","INSERT COMBINADO" + combinados.IDProduto)
            } catch (e: Exception) {
                Log.i("ROOM_SQLITE","ERRO INSERT OPCAO" + e.printStackTrace())
            }
        }
    }

    override fun insertProdutoOpcao(settingsResponse: Response<List<SettingsResponse>>) {
        //insert produtoOpcao
        val listObservable = Observable.fromArray(settingsResponse.body()[5].ProdutoOpcao)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var produtoOpcao = ProdutoOpcao()
                produtoOpcao.Descricao = value.descricao.toLowerCase()
                produtoOpcao.IDProduto = value.idProduto
                produtoOpcao.IDProdutoOpcao = value.idProdutoOpcao
                produtoOpcao.Ordem = value.ordem

                apply {
                    ComandaApplication.database.produtoOpcao().insert(produtoOpcao)
                }

                Log.i("ROOM_SQLITE","INSERT PRODUTO_OPCAO" + produtoOpcao.IDProdutoOpcao)
            } catch (e: Exception) {
                Log.i("ROOM_SQLITE","ERRO INSERT PRODUTO_OPCAO" + e.printStackTrace())
            }
        }
    }
}