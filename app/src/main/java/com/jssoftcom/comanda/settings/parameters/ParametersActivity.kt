package com.jssoftcom.comanda.settings.parameters

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.URLUtil
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.Loading
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.settings.parameters.ParametersPreference.Companion.IDENTIFICA_GARCOM
import com.jssoftcom.comanda.settings.parameters.ParametersPreference.Companion.ID_GARCOM
import com.jssoftcom.comanda.settings.parameters.ParametersPreference.Companion.NAME_DEVICE
import com.jssoftcom.comanda.settings.parameters.ParametersPreference.Companion.SENHA_GARCOM
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract
import kotlinx.android.synthetic.main.activity_parameters.*
import retrofit2.Response

class ParametersActivity : BaseActivity(), ParametersContract.View, ParametersListenersContract.parametersListenerContract{

    lateinit var presenter: ParametersContract.Presenter
    lateinit var loading : Loading

    override fun startPresenter() {
        presenter = ParametersPresenter(this)
        presenter.start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parameters)

        close.setOnClickListener {
            onBackPressed()
        }

        btnSave.setOnClickListener {
            var url : String = editUrl.text.toString()

            if (url.isEmpty() && !URLUtil.isValidUrl(url)) {
                showToast("url incorreta")
            } else {

                if (url.last().toString().equals("/")){
                    ParametersPreference.instance.saveUrl(this, "url", url)
                } else {
                    ParametersPreference.instance.saveUrl(this, "url", "$url/")
                }

                if (application.setBaseUrl()){
                    startPresenter()

                    loading = Loading(this)
                    loading.show()

                    presenter.wsAtivo(this)
                }

                ParametersPreference.instance.saveNameDevice(this, ParametersPreference.NAME_DEVICE, edtNameDevice.text.toString())
            }
        }

        var identificaGarcom = ParametersPreference.instance.getIdentificaGarcom(this, ParametersPreference.IDENTIFICA_GARCOM)

        if (identificaGarcom){
            switch1.isChecked = true
        }

        switch1.setOnClickListener {
            if (switch1.isChecked){
                ParametersPreference.instance.saveIdentificaGarcom(this, IDENTIFICA_GARCOM, true)
            } else {
                ParametersPreference.instance.saveIdentificaGarcom(this, IDENTIFICA_GARCOM, false)
                ParametersPreference.instance.saveIdGarcom(this, ID_GARCOM, "")
            }
        }

        var url = ParametersPreference.instance.getUrl(this, "url")
        editUrl.setText(url)

        var nameDevice = ParametersPreference.instance.getNameDevice(this, NAME_DEVICE)
        edtNameDevice.setText(nameDevice)
    }

    override fun success(parametersResponseResponse: Response<List<ParametersResponse>>?) {
        loading.dismiss()
        var response = parametersResponseResponse?.body()

        if (response?.get(0)?.wsOnLine?.get(0)?.ativo == "1"){
            runOnUiThread{
                showToast("URl configurada com sucesso")

                if (!switch1.isChecked){
                    onBackPressed()
                }
            }
        }
    }

    override fun erroMessage() {
        loading.dismiss()
        showToast("Falha na requisição")
    }

    override fun dataInvalid() {
        loading.dismiss()
        showToast("Falha na requisição")
    }

    override fun failure(t: Throwable?) {
        loading.dismiss()
        showToast("Falha na requisição")
    }



}