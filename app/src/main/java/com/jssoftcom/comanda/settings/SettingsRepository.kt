package com.jssoftcom.comanda.settings

import com.jssoftcom.comanda.settings.domain.data.remote.SettingsListenerContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosListenersContract

/**
 * Created by gustavon on 02/11/17.
 */
class SettingsRepository(var settingsRemoteRepository: SettingsRemoteRepository) {

    fun produtos(produtosListenersContract: ProdutosListenersContract) {
        settingsRemoteRepository.produtos(produtosListenersContract)
    }


    fun categorias(settingsListenerContract: SettingsListenerContract) {
        settingsRemoteRepository.categorias(settingsListenerContract)
    }

}