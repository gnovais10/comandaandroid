package com.jssoftcom.comanda.settings.parameters;

import java.util.List;

/**
 * Created by gustavon on 30/10/17.
 */

public class ParametersResponse {

    private List<WsOnLine> wsOnLine;

    public List<WsOnLine> getWsOnLine() {
        return wsOnLine;
    }

    public void setWsOnLine(List<WsOnLine> wsOnLine) {
        this.wsOnLine = wsOnLine;
    }

    public static class WsOnLine {
        /**
         * Ativo : 1
         */

        private String Ativo;

        public String getAtivo() {
            return Ativo;
        }

        public void setAtivo(String Ativo) {
            this.Ativo = Ativo;
        }
    }
}
