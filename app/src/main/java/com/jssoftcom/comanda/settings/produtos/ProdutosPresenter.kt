package com.jssoftcom.comanda.settings.produtos

import android.content.Context
import android.util.Log
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.database.tables.Produto
import com.jssoftcom.comanda.settings.SettingsRemoteRepository
import com.jssoftcom.comanda.settings.SettingsRepository
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosListenersContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse
import io.reactivex.Observable
import retrofit2.Response

/**
 * Created by gustavon on 02/11/17.
 */
class ProdutosPresenter(var context: Context) : ProdutosContract.Presenter {

    var parametersRepository: SettingsRepository? = null

    override fun start() {
        parametersRepository = SettingsRepository(SettingsRemoteRepository(context))
    }

    override fun produtos(produtosListenersContract: ProdutosListenersContract) {
        parametersRepository!!.produtos(produtosListenersContract)
    }

    override fun deleteAllProdutos() {
        ComandaApplication.database?.produtosDao()?.deleteAll()
    }

    override fun insertProdutos(produtosResponse: Response<List<ProdutosResponse>>) {
        var listProd = produtosResponse.body().get(0)

        val listObservable = Observable.fromArray(listProd.produtos)

        listObservable.flatMap { values ->
            Observable.fromIterable(values)
        }.subscribe { value ->
            try {
                var produto = Produto()
                produto.IDProduto = value.idProduto
                produto.Descricao = value.descricao.toLowerCase()
                produto.Preco = value.preco
                produto.Combinado = value.combinado
                produto.IDCategoria = value.idCategoria
                produto.IDSubCategoria = value.idSubCategoria
                produto.SubCategoria = value.subCategoria
                produto.Ordem = value.ordem
                produto.MultiploVenda = value.multiploVenda
                produto.DigitaQuantidade = value.digitaQuantidade

                ComandaApplication.database?.produtosDao()?.insert(produto)

                Log.i("ROOM_SQLITE","INSERT PRODUTO" + produto.IDProduto)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}