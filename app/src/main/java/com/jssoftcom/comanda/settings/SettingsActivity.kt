package com.jssoftcom.comanda.settings

import android.content.Intent
import android.os.Bundle
import com.jssoftcom.comanda.*
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import com.jssoftcom.comanda.settings.domain.data.remote.SettingsContract
import com.jssoftcom.comanda.settings.domain.data.remote.SettingsListenerContract
import com.jssoftcom.comanda.settings.domain.data.remote.SettingsPresenter
import com.jssoftcom.comanda.settings.parameters.ParametersActivity
import com.jssoftcom.comanda.settings.produtos.ProdutosContract
import com.jssoftcom.comanda.settings.produtos.ProdutosPresenter
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosListenersContract
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse
import kotlinx.android.synthetic.main.activity_settings.*
import retrofit2.Response


class SettingsActivity : BaseActivity(), ProdutosContract.View, SettingsContract.View, ProdutosListenersContract, SettingsListenerContract {

    lateinit var produtosPresenter: ProdutosContract.Presenter
    lateinit var presenter: SettingsContract.Presenter
    lateinit var loading: Loading

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        startPresenter()
        configBack()
        configParameters()
        loadTables()
        configStart()
    }

    override fun startPresenter() {
        produtosPresenter = ProdutosPresenter(this)
        produtosPresenter.start()

        presenter = SettingsPresenter(this)
        presenter.start()
    }

    fun configStart(){
        iniciar.setOnClickListener{
            onBackPressed()
        }
    }

    fun loadTables() {
        loadTables.setOnClickListener {
            if (application.setBaseUrl()){
                application.setBaseUrl();

                loading = Loading(this)
                loading.show()

                produtosPresenter.produtos(this)
                presenter.categorias(this);
            }
        }
    }

    fun configBack() {
        back.setOnClickListener {
            onBackPressed()
        }
    }

    fun configParameters() {
        cardParameters.setOnClickListener {
            startActivity(Intent(this, ParametersActivity::class.java))
        }
    }

    override fun success(produtosResponse: Response<List<ProdutosResponse>>) {
        if (produtosResponse.isSuccessful) {
            // deleta produtos existentes
            produtosPresenter.deleteAllProdutos()

            produtosPresenter.insertProdutos(produtosResponse)
        }
    }

    override fun successSettings(settingsResponse: Response<List<SettingsResponse>>) {
        if (settingsResponse.isSuccessful) {
            //deleta os itens exisites
            presenter.deleteAll()

            presenter.insertCategorias(settingsResponse)
            presenter.insertSubCategorias(settingsResponse)
            presenter.insertOpcao(settingsResponse)
            presenter.insertCombinados(settingsResponse)
            presenter.insertProdutoOpcao(settingsResponse)


            runOnUiThread {
                loading.dismiss()
            }
        }
    }

    override fun erroMessage() {
        showToast("Falha na requisição")
        runOnUiThread {
            loading.dismiss()
        }
    }

    override fun dataInvalid() {
        showToast("Falha na requisição")
        runOnUiThread {
            loading.dismiss()
        }
    }

    override fun failure(t: Throwable) {
        showToast("Falha na requisição")
        runOnUiThread {
            loading.dismiss()
        }
    }
}
