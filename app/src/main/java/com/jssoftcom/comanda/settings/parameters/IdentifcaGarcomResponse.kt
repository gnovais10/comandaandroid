package com.jssoftcom.comanda.settings.parameters

import com.google.gson.annotations.SerializedName

/**
 * Created by gustavon on 02/12/17.
 */
class IdentifcaGarcomResponse {

    @SerializedName("IDGarcom")
    lateinit var IDGarcom : String

    @SerializedName("Nome")
    lateinit var Nome : String
}