package com.jssoftcom.comanda.settings.parameters.domain.data.remote;

/**
 * Created by GustavoNovais on 27/04/17.
 */

public class ParametersRepository {

    private ParametersRemoteRepository remoteRepository;

    public ParametersRepository(ParametersRemoteRepository parametersRemoteRepository) {
        this.remoteRepository = parametersRemoteRepository;
    }

    public void wsAtivo(ParametersListenersContract.parametersListenerContract parametersListenerContract) {
        remoteRepository.wsAtivo(parametersListenerContract);
    }

}
