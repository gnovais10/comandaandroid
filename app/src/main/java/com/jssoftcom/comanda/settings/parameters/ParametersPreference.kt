package com.jssoftcom.comanda.settings.parameters

import android.content.Context
import com.jssoftcom.comanda.ComandaPreferences

/**
 * Created by gustavon on 30/10/17.
 */
class ParametersPreference : ComandaPreferences() {

    private object Holder {
        internal val INSTANCE = ParametersPreference()
    }

    fun saveUrl(context: Context, key: String,  url: String) {
        savePreference(context, key, url)
    }


    fun getUrl(context: Context, url: String): String {
        return getStringPreference(context, url, "")
    }

    fun saveIdentificaGarcom(context: Context, key: String,  identificaGarcom: Boolean) {
        savePreference(context, key, identificaGarcom)
    }

    fun getIdentificaGarcom(context: Context, key: String): Boolean {
        return getBooleanPreference(context, key, false)
    }
/*
    fun getPasswordGarcom(context: Context, key: String): String {
        return getStringPreference(context, key, "")
    }

    fun savePasswordGarcom(context: Context, key: String,  url: String) {
        savePreference(context, key, url)
    }*/

    fun saveIdGarcom(context: Context, key: String,  url: String) {
        savePreference(context, key, url)
    }

    fun getIdGarcom(context: Context, key: String): String {
        return getStringPreference(context, key, "")
    }

    fun garcomLogado(context: Context, key: String,  garcomLogado: Boolean) {
        savePreference(context, key, garcomLogado)
    }

    fun getGarcomLogado(context: Context, key: String): Boolean {
        return getBooleanPreference(context, key, false)
    }

    fun saveNameDevice(context: Context, key: String,  url: String) {
        savePreference(context, key, url)
    }

    fun getNameDevice(context: Context, key: String): String {
        return getStringPreference(context, key, "")
    }

    companion object {
        val instance: ParametersPreference
            get() = Holder.INSTANCE
        const val IDENTIFICA_GARCOM = "IDENTIFICA_GARCOM"
        const val SENHA_GARCOM = "SENHA_GARCOM"
        const val ID_GARCOM = "ID_GARCOM"
        const val GARCOM_LOGADO = "GARCOM_LOGADO"
        const val NAME_DEVICE = "NAME_DEVICE"
    }

}