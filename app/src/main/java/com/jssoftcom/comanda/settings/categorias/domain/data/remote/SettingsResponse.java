package com.jssoftcom.comanda.settings.categorias.domain.data.remote;

import android.os.Parcel;
import android.os.Parcelable;

import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gustavon on 03/11/17.
 */

public class SettingsResponse {

    public List<Categorias> Categorias;
    public List<SubCategorias> SubCategorias;
    public List<Opcao> Opcao;
    public List<Combinados> Combinados;
    public List<ProdutoOpcao> ProdutoOpcao;

    public static class Categorias {
        //campo usado para montar a lista com subitens
        public ArrayList<SubCategorias> SubCategorias = new ArrayList<>();

        private String IDCategoria;
        private String Descricao;
        private String Imagem;
        private String CategoriaPizza;
        private String Ordem;

        public String getIDCategoria() {
            return IDCategoria;
        }

        public void setIDCategoria(String IDCategoria) {
            this.IDCategoria = IDCategoria;
        }

        public String getDescricao() {
            return Descricao;
        }

        public void setDescricao(String Descricao) {
            this.Descricao = Descricao;
        }

        public String getImagem() {
            return Imagem;
        }

        public void setImagem(String Imagem) {
            this.Imagem = Imagem;
        }

        public String getCategoriaPizza() {
            return CategoriaPizza;
        }

        public void setCategoriaPizza(String CategoriaPizza) {
            this.CategoriaPizza = CategoriaPizza;
        }

        public String getOrdem() {
            return Ordem;
        }

        public void setOrdem(String Ordem) {
            this.Ordem = Ordem;
        }
    }

    public static class SubCategorias {
        //campo usado para montar a lista com subitens
        public List<ProdutosResponse.Produtos> produtos = new ArrayList<>();

        private String IDSubCategoria;
        private String Descricao;
        private String Ordem;

        public String getIDSubCategoria() {
            return IDSubCategoria;
        }

        public void setIDSubCategoria(String IDSubCategoria) {
            this.IDSubCategoria = IDSubCategoria;
        }

        public String getDescricao() {
            return Descricao;
        }

        public void setDescricao(String Descricao) {
            this.Descricao = Descricao;
        }

        public String getOrdem() {
            return Ordem;
        }

        public void setOrdem(String Ordem) {
            this.Ordem = Ordem;
        }
    }

    public static class Opcao implements Parcelable{
        /**
         * IDOpcao : 1
         * IDCategoria : 13
         * Descricao : COM GELO
         * Ordem : 1
         */

        private String IDOpcao;
        private String IDCategoria;
        private String Descricao;
        private String Ordem;

        public Opcao() {
        }

        protected Opcao(Parcel in) {
            IDOpcao = in.readString();
            IDCategoria = in.readString();
            Descricao = in.readString();
            Ordem = in.readString();
        }

        public static final Creator<Opcao> CREATOR = new Creator<Opcao>() {
            @Override
            public Opcao createFromParcel(Parcel in) {
                return new Opcao(in);
            }

            @Override
            public Opcao[] newArray(int size) {
                return new Opcao[size];
            }
        };

        public String getIDOpcao() {
            return IDOpcao;
        }

        public void setIDOpcao(String IDOpcao) {
            this.IDOpcao = IDOpcao;
        }

        public String getIDCategoria() {
            return IDCategoria;
        }

        public void setIDCategoria(String IDCategoria) {
            this.IDCategoria = IDCategoria;
        }

        public String getDescricao() {
            return Descricao;
        }

        public void setDescricao(String Descricao) {
            this.Descricao = Descricao;
        }

        public String getOrdem() {
            return Ordem;
        }

        public void setOrdem(String Ordem) {
            this.Ordem = Ordem;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(IDOpcao);
            parcel.writeString(IDCategoria);
            parcel.writeString(Descricao);
            parcel.writeString(Ordem);
        }
    }

    public static class Combinados {
        /**
         * IDProduto : 7
         * IDProdutoReferente : 17
         */

        private String IDProduto;
        private String IDProdutoReferente;

        public String getIDProduto() {
            return IDProduto;
        }

        public void setIDProduto(String IDProduto) {
            this.IDProduto = IDProduto;
        }

        public String getIDProdutoReferente() {
            return IDProdutoReferente;
        }

        public void setIDProdutoReferente(String IDProdutoReferente) {
            this.IDProdutoReferente = IDProdutoReferente;
        }
    }

    public static class ProdutoOpcao {
        /**
         * IDProdutoOpcao : 1
         * IDProduto : 74
         * Descricao : COM GELO
         * Ordem : 1
         */

        private String IDProdutoOpcao;
        private String IDProduto;
        private String Descricao;
        private String Ordem;

        public String getIDProdutoOpcao() {
            return IDProdutoOpcao;
        }

        public void setIDProdutoOpcao(String IDProdutoOpcao) {
            this.IDProdutoOpcao = IDProdutoOpcao;
        }

        public String getIDProduto() {
            return IDProduto;
        }

        public void setIDProduto(String IDProduto) {
            this.IDProduto = IDProduto;
        }

        public String getDescricao() {
            return Descricao;
        }

        public void setDescricao(String Descricao) {
            this.Descricao = Descricao;
        }

        public String getOrdem() {
            return Ordem;
        }

        public void setOrdem(String Ordem) {
            this.Ordem = Ordem;
        }
    }
}
