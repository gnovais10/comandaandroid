package com.jssoftcom.comanda.settings.parameters.domain.data.remote;

import android.os.Build;

import com.example.jssoftcom.comanda.infra.ComandaAndroidAPI;
import com.jssoftcom.comanda.BuildConfig;
import com.jssoftcom.comanda.ComandaApplication;
import com.jssoftcom.comanda.infra.RetrofitProvider;
import com.jssoftcom.comanda.infra.handleError.MyCallBack;
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault;
import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse;
import com.jssoftcom.comanda.settings.parameters.ParametersResponse;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Response;

/**
 * Created by GustavoNovais on 27/04/17.
 */

public class ParametersRemoteRepository {

    private ComandaAndroidAPI remoteRepository;

    public ParametersRemoteRepository() {
        remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI.class);
    }

    public void wsAtivo(final ParametersListenersContract.parametersListenerContract listener) {
        remoteRepository.wsAtivo().enqueue(new MyCallBack<List<ParametersResponse>>() {
            @Override
            public void success(Response<List<ParametersResponse>> response) {
                listener.success(response);
            }

            @Override
            public void unauthenticated(Response<?> response) {
                listener.erroMessage();
            }

            @Override
            public void clientError(Response<?> response) {
                listener.erroMessage();
            }

            @Override
            public void serverError(Response<?> response) {
                listener.erroMessage();
            }

            @Override
            public void networkError(IOException e) {
                listener.erroMessage();
            }

            @Override
            public void unexpectedError(Throwable t) {
                listener.failure(t);
            }
        });
    }




}
