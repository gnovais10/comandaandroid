package com.jssoftcom.comanda.settings.produtos.domain.data.remote

import retrofit2.Response

/**
 * Created by gustavon on 02/11/17.
 */
interface ProdutosListenersContract {
    fun success(produtosResponse: Response<List<ProdutosResponse>>)

    fun erroMessage()

    fun dataInvalid()

    fun failure(t: Throwable)
}