package com.jssoftcom.comanda.settings.parameters;

import android.content.Context;

import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract;
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersRemoteRepository;
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersRepository;


/**
 * Created by GustavoNovais on 26/04/17.
 */

public class ParametersPresenter implements ParametersContract.Presenter {

    private ParametersRepository parametersRepository;
    private Context context;

    public ParametersPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void start() {
        parametersRepository = new ParametersRepository(new ParametersRemoteRepository());
    }

    @Override
    public void wsAtivo(ParametersListenersContract.parametersListenerContract parametersListenerContract) {
        parametersRepository.wsAtivo(parametersListenerContract);
    }


}
