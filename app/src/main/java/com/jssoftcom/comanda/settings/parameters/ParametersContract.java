package com.jssoftcom.comanda.settings.parameters;

import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract;

/**
 * Created by GustavoNovais on 26/04/17.
 */

public interface ParametersContract {

    interface View {
        void startPresenter();
    }

    interface Presenter {
        void wsAtivo(ParametersListenersContract.parametersListenerContract parametersListenerContract);
        void start();
    }

}
