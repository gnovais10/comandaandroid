package com.jssoftcom.comanda.settings.categorias.domain.data.remote

/**
 * Created by gustavon on 03/11/17.
 */
open class SubCategoria {
    lateinit var idSubCategoria: String
    lateinit var descricao: String
    lateinit var ordem: String
}