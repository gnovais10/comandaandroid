package com.jssoftcom.comanda.settings.categorias.domain.data.remote

import com.google.gson.annotations.SerializedName

/**
 * Created by gustavon on 03/11/17.
 */

open class Combinado {
    lateinit var idProduto: String
    lateinit var idProdutoReferente: String
}