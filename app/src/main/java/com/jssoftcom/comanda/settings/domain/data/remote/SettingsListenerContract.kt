package com.jssoftcom.comanda.settings.domain.data.remote

import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import retrofit2.Response

/**
 * Created by gustavon on 03/11/17.
 */
interface SettingsListenerContract {
    fun successSettings(settingsResponse: Response<List<SettingsResponse>>)

    fun erroMessage()
}