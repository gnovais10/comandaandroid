package com.jssoftcom.comanda.settings.parameters.domain.data.remote;

import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault;
import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse;
import com.jssoftcom.comanda.settings.parameters.ParametersResponse;

import java.util.List;

import retrofit2.Response;

/**
 * Created by GustavoNovais on 27/04/17.
 */

public class ParametersListenersContract {

    public interface parametersListenerContract {
        void success(Response<List<ParametersResponse>> parametersResponseResponse);
        void erroMessage();
        void dataInvalid();
        void failure(Throwable t);
    }

    public interface validarLicencaListenerContract {
        void success(Response<ResponseDefault> response);
        void erroMessage();
    }
}
