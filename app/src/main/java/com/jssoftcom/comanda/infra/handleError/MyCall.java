package com.jssoftcom.comanda.infra.handleError;

/**
 * Created by GustavoNovais on 05/05/17.
 */

public interface MyCall<T> {
    void cancel();
    void enqueue(MyCallBack<T> callback);
    MyCall<T> clone();
}
