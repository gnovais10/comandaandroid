package com.example.jssoftcom.comanda.infra

import com.jssoftcom.comanda.infra.handleError.MyCall
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.domain.data.remote.ResponsePreConta
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse
import com.jssoftcom.comanda.settings.parameters.ParametersResponse
import com.jssoftcom.comanda.settings.produtos.domain.data.remote.ProdutosResponse
import retrofit2.http.GET
import retrofit2.http.QueryMap

/**
 * Created by gustavon on 30/10/17.
 */
interface ComandaAndroidAPI {

    @GET("wsonline")
    fun wsAtivo(): MyCall<List<ParametersResponse>>

    @GET("produtos")
    fun produtos(): MyCall<List<ProdutosResponse>>

    @GET("Auxiliares")
    fun categorias(): MyCall<List<SettingsResponse>>

    @GET("AbreMesa")
    fun abreMesa(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>

    @GET("AdicionaProduto")
    fun addProduto(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>

    @GET("FinalizarPedido")
    fun finalizarPedido(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>

    @GET("RemoveProduto")
    fun removeProduto(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>

    @GET("visualizaPreConta")
    fun visualizaPreConta(@QueryMap params : Map<String, String>): MyCall<List<ResponsePreConta>>

    @GET("wsidentificagarcom")
    fun identificaGarcom(@QueryMap params : Map<String, String>): MyCall<IdentifcaGarcomResponse>

    @GET("ValidarLicencas")
    fun validarLicencas(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>


    @GET("EmitePreConta")
    fun emitirPreConta(@QueryMap params : Map<String, String>): MyCall<ResponseDefault>

}