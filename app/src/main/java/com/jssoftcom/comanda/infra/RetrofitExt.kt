package com.jssoftcom.comanda.infra

import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.jssoftcom.comanda.infra.handleError.ErrorHandlingCallAdapterFactory
import com.jssoftcom.comanda.settings.parameters.ParametersActivity
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by gustavon on 05/11/17.
 */

object RetrofitProvider {
    val httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)

    val logging = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    val client = httpClient.addInterceptor(logging).build()


    var retrofitCreated = false

    lateinit var retrofit: Retrofit
}

fun Context.setBaseUrl() : Boolean {
    val url = ParametersPreference.instance.getUrl(this, "url")

    try {
        if (!url.isEmpty() || !RetrofitProvider.retrofitCreated){
            RetrofitProvider.retrofit = Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(ErrorHandlingCallAdapterFactory())
                    .client(RetrofitProvider.client)
                    .build()

            RetrofitProvider.retrofitCreated = true
            return true
        } else {
            Toast.makeText(this, "configure o enderenço do servidor", Toast.LENGTH_LONG).show()
            startActivity(Intent(this, ParametersActivity::class.java))
            return false
        }
    }catch (e : Exception){
        e.printStackTrace()
        Toast.makeText(this, "configure o enderenço do servidor", Toast.LENGTH_LONG).show()
        return false
    }
}