package com.jssoftcom.comanda.ordersummary.dto

import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse

/**
 * Created by gustavon on 05/11/17.
 */
class OrderSummayList {
    var listCategorias = ArrayList<SettingsResponse.Categorias>()
}