package com.jssoftcom.comanda.ordersummary

import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderDeleteItemListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderRemoteRepository
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderRepository

/**
 * Created by gustavon on 16/11/17.
 */
class OrderSummaryPresenter : OrderSummaryContract.Presenter {

    lateinit var orderRepository: OrderRepository

    override fun start() {
        orderRepository = OrderRepository(OrderRemoteRepository())
    }


    override fun finalizaPedido(orderListenerContract: OrderListenerContract, nrMesa: String, nrCartao: String) {
        orderRepository.finalizarPedido(orderListenerContract, nrMesa, nrCartao)
    }

    override fun deletarItem(orderListenerContract: OrderDeleteItemListenerContract, nrMesa: String, nrCartao: String, idProduto: String){
        orderRepository.deletarItem(orderListenerContract, nrMesa, nrCartao, idProduto)
    }



}