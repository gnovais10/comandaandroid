package com.jssoftcom.comanda.ordersummary.adapter

import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.R.id.checkItemObs
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import android.widget.CompoundButton




/**
 * Created by gustavon on 11/11/17.
 */
open class ObsAdapter(val items: List<SettingsResponse.Opcao>) : RecyclerView.Adapter<ObsAdapter.ViewHolder>() {

    companion object {
        var opcoesSelecionadas =  mutableListOf<String>()
        var opcoesSelecionadasObj =  mutableListOf<SettingsResponse.Opcao>()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_obs_check, parent, false)

        return ViewHolder(v)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.checkItemObs.text = item.descricao
        holder.checkItemObs.tag = item.idOpcao
        holder.itemView.tag = item
    }


    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var checkItemObs: CheckBox

        init {
            checkItemObs = itemView.findViewById(R.id.checkItemObs)

            checkItemObs.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->

                if (isChecked){
                    ObsAdapter.opcoesSelecionadas.add(checkItemObs.tag.toString())
                    ObsAdapter.opcoesSelecionadasObj.add(itemView.tag as SettingsResponse.Opcao)
                } else {
                    ObsAdapter.opcoesSelecionadasObj.remove(itemView.tag as SettingsResponse.Opcao)
                    ObsAdapter.opcoesSelecionadas.remove(checkItemObs.tag.toString())
                }

            }
            )

        }

    }

}
