package com.jssoftcom.comanda.ordersummary

import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderDeleteItemListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract

/**
 * Created by gustavon on 16/11/17.
 */
interface OrderSummaryContract {

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun start()
        fun finalizaPedido(orderListenerContract: OrderListenerContract, nrMesa: String, nrCartao: String)
        fun deletarItem(orderListenerContract: OrderDeleteItemListenerContract, nrMesa: String, nrCartao: String, idProduto : String)
    }

}