package com.jssoftcom.comanda.ordersummary

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.model.Item
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.order_summary_item.view.*


class OrderSummaryAdapter(items: ArrayList<Item>) : RecyclerView.Adapter<OrderSummaryAdapter.OrderSummaryViewHolder>() {

    var orderSummaryItems: ArrayList<Item> = items

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OrderSummaryViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.order_summary_item, parent, false)
        return OrderSummaryViewHolder(view)
    }

    override fun onBindViewHolder(holder: OrderSummaryViewHolder?, position: Int) {
        val item = orderSummaryItems.get(position)

        holder?.itemQuantity?.setText("${item.quantity}")
        holder?.itemDescription?.setText(item.description)

        setDescOpcoes(item, holder!!)

        if (item.quantity > 1) {
            var valor: Float = item.value.replace(",", ".").toFloat() * item.quantity
            holder?.itemValue?.setText(valor.toString().replace(".", "."))
        } else {
            holder?.itemValue?.setText(item.value)
        }

    }

    fun setDescOpcoes(item: Item, holder: OrderSummaryViewHolder) {
        var descTotalOpcoes = String()

        for (item1 in item.opcoes) {
            item.opcoes.forEach {
              if (item1.idOpcao == it.idOpcao){
                  if (!item.opcoes.last().equals(item1)) {
                      descTotalOpcoes += "${item1.descricao.toLowerCase()} | "
                  } else {
                      descTotalOpcoes += "${item1.descricao.toLowerCase()}"
                  }
              }
            }
        }

        if (!descTotalOpcoes.isEmpty() && !item.obsItem.isEmpty()) {
            holder.descOpcoes.text = "$descTotalOpcoes | ${item.obsItem}"
        } else if (!item.obsItem.isEmpty()){
            holder.descOpcoes.text = "${item.obsItem}"
        } else {
            holder.descOpcoes.text = "$descTotalOpcoes"
        }

    }

    override fun getItemCount(): Int {
        return orderSummaryItems.size
    }


    class OrderSummaryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val itemQuantity = view.itemQuantity
        val itemDescription = view.itemDescription
        val itemValue = view.itemValue
        val viewForeground = view.viewForeground
        val descOpcoes = view.descOpcoes

    }

    fun removeItem(position: Int) {
        orderSummaryItems.removeAt(position)
        notifyItemRemoved(position)
    }
}