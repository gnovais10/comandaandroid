package com.jssoftcom.comanda.ordersummary

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.OrderClosedFragment
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderDeleteItemListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract
import com.jssoftcom.comanda.ordersummary.OrderSummaryAdapter.OrderSummaryViewHolder
import kotlinx.android.synthetic.main.order_summary_fragment.*
import retrofit2.Response

class OrderSummaryFragment : Fragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, OrderSummaryContract.View, OrderListenerContract,OrderDeleteItemListenerContract {
    var adapter = OrderSummaryAdapter(arrayListOf())

    var selectedItems : ArrayList<Item> = arrayListOf()

    lateinit var orderSummaryPresenter : OrderSummaryPresenter

    lateinit var nrMesa : String
    lateinit var nrCartao : String

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.order_summary_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments.getParcelableArrayList<Item>("SELECTED_ITEMS") != null){
            selectedItems = arguments.getParcelableArrayList<Item>("SELECTED_ITEMS")
        }

        configClose()
        configItemsList(selectedItems)
        configBack()
        getInfoTable()
        configClose()
        confirmCloseOrder()
        startPresenter()
    }

    fun getInfoTable(){
        val bundle = this.arguments
        if (bundle != null) {
            nrMesa = bundle.getString(OpenTableFragment.NRMESA, "")
            nrCartao = bundle.getString(OpenTableFragment.NRCARTAO, "")
        }
    }


    fun configBack(){
        closeButton.setOnClickListener{
            activity.onBackPressed()
        }
    }
    override fun startPresenter() {
        orderSummaryPresenter = OrderSummaryPresenter()
        orderSummaryPresenter.start()
    }

    fun confirmCloseOrder(){
        relativeConfimPreAccount.setOnClickListener{
            orderSummaryPresenter.finalizaPedido(this, nrMesa, nrCartao)
        }
    }

    override fun successDeleteItem(settingsResponse: Response<ResponseDefault>) {

    }

    override fun success(settingsResponse: Response<ResponseDefault>) {
        activity.supportFragmentManager.beginTransaction().replace(R.id.orderContainer, OrderClosedFragment()).commit()
    }

    override fun erroMessage() {
        Snackbar.make(order_summary, "Falha na requisição", Snackbar.LENGTH_SHORT).show()
    }

    private fun configItemsList(items: ArrayList<Item>) {

        val itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)
        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(orderSummaryItems)

        orderSummaryItems.layoutManager = LinearLayoutManager(activity)

        var itens = selectedItems

        adapter = OrderSummaryAdapter(itens)
        orderSummaryItems.adapter = adapter
        adapter.notifyDataSetChanged()


        //valor total dos itens
        apply {
            var valor : Float = 0.0f
            for (item in selectedItems){

                if (item.quantity > 1){
                    valor += item.value.replace(",",".").toFloat() * item.quantity
                } else {
                    valor += item.value.replace(",",".").toFloat()
                }
            }

            totalValue.setText("%.2f".format(valor))
        }
    }


    fun configClose(){
        closeButton.setOnClickListener{
            activity.onBackPressed()
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is OrderSummaryViewHolder) {

            activity.setBaseUrl()
            orderSummaryPresenter.deletarItem(this, nrMesa, nrCartao, selectedItems.get(position).idProduto)

            //atualizar campo do valor
            var valor = totalValue.text.toString().replace(",",".").toFloat()
            valor -= selectedItems.get(position).value.replace(",",".").toFloat()
            totalValue.setText("%.2f".format(valor))

            adapter.removeItem(viewHolder.getAdapterPosition());

            if (adapter.orderSummaryItems.size == 0 ){
                relativeConfimPreAccount.visibility = View.GONE
                (activity as BaseActivity).showToast("selecione produtos para finalizar o pedido!")
                activity.onBackPressed()
            }
        }
    }
}