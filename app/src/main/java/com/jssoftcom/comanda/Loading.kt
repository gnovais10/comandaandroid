package com.jssoftcom.comanda

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle

/**
 * Created by gustavon on 04/11/17.
 */

class Loading(context: Context) : Dialog(context) {

    override fun onSaveInstanceState(): Bundle {
        return super.onSaveInstanceState()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(onSaveInstanceState())
        this.setContentView(R.layout.progress)

        this.setCancelable(false)
        this.setCanceledOnTouchOutside(false)

        apply {
            this.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun show() {
        if (!this.isShowing) {
            super.show()
        }
    }

    override fun dismiss() {
        if (this.isShowing) {
            super.dismiss()
        }
    }

    override fun cancel() {
        if (this.isShowing) {
            super.cancel()
        }
    }
}