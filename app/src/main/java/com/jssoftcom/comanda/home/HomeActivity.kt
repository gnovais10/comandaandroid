package com.jssoftcom.comanda.home

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.order.OpenTableContract
import com.jssoftcom.comanda.order.OpenTablePresenter
import com.jssoftcom.comanda.order.ativities.OrderActivity
import com.jssoftcom.comanda.order.ativities.SelectTablePreAccountActivity
import com.jssoftcom.comanda.order.domain.data.remote.OpenTableListenerContract
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.settings.SettingsActivity
import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import com.jssoftcom.comanda.settings.parameters.ParametersPreference.Companion.GARCOM_LOGADO
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Response


/**
 * Created by gustavon on 27/10/17.
 */
class HomeActivity : BaseActivity(), OpenTableListenerContract.identificaGarcomListenerContract, ParametersListenersContract.validarLicencaListenerContract{

    lateinit var presenter : OpenTableContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initFont()

        presenter = OpenTablePresenter()
        presenter.start()


        cardViewSettings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }

        newOrderClick()

        cardViewPreAccount.setOnClickListener {
            if (verifyBaseUrl()){
                var intent = Intent(this, SelectTablePreAccountActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                startActivity(intent)
            }
        }
    }

    private fun newOrderClick() {
        cardViewNewOrder.setOnClickListener {
            validarLicenca()
        }
    }

    fun openTable(){
        runOnUiThread {
            var identificaGarcom = ParametersPreference.instance.getIdentificaGarcom(this, ParametersPreference.IDENTIFICA_GARCOM)
            var garcomLogado = ParametersPreference.instance.getGarcomLogado(this, GARCOM_LOGADO)

            if (identificaGarcom) {
                if (!garcomLogado) {
                    var alert: AlertDialog

                    val builder = AlertDialog.Builder(this)
                    builder.setView(R.layout.senha_garcom_dialog)

                    alert = builder.create()
                    alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert.show()

                    var btnRight = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_right)
                    var editSenhaGarcom = alert.window.decorView.findViewById<EditText>(R.id.edtSenhaGarcom)
                    btnRight.setOnClickListener {
                        alert.dismiss()
                        setBaseUrl()
                        presenter.identificaGarcom(this, editSenhaGarcom.text.toString())
                    }

                    var btnLeft = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_left)
                    btnLeft.setOnClickListener {
                        alert.dismiss()
                    }
                } else {
                    if (verifyBaseUrl()) {
                        startActivity(Intent(this, OrderActivity::class.java))
                    }
                }
            } else {
                if (verifyBaseUrl()) {
                    startActivity(Intent(this, OrderActivity::class.java))
                }
            }
        }
    }

    fun verifyBaseUrl() : Boolean {
        var urlAndBaseOk = true

        var url = ParametersPreference.instance.getUrl(this, "url")

        if (url.isEmpty()){
            showToast("configure a url do servidor e clique em atualizar base de dados")
            startActivity(Intent(this, SettingsActivity::class.java))
            urlAndBaseOk =  false
        }
            var sizeCategoria : Int = 0

                ComandaApplication.database.categoriasDao().getCategorias()
                        .subscribeOn(Schedulers.io())
                        ?.observeOn(AndroidSchedulers.mainThread())
                        ?.subscribe { values ->
                            sizeCategoria = values.size

                            if (sizeCategoria == 0 ){
                                showToast("configure a url do servidor e clique em atualizar base de dados")
                                startActivity(Intent(this, SettingsActivity::class.java))

                                urlAndBaseOk = false
                            }
                        }

        return urlAndBaseOk
    }

    override fun successIdentifcaGarcom(response: Response<IdentifcaGarcomResponse>) {
        runOnUiThread{
            if (response?.body()?.IDGarcom != "0"){
                ParametersPreference.instance.garcomLogado(this, GARCOM_LOGADO, true)
                ParametersPreference.instance.saveIdentificaGarcom(this, ParametersPreference.IDENTIFICA_GARCOM, true)
                ParametersPreference.instance.saveIdGarcom(this, ParametersPreference.ID_GARCOM, response?.body()?.IDGarcom!!)

                if (verifyBaseUrl()){
                    startActivity(Intent(this, OrderActivity::class.java))
                }
            } else {
                showToast("informe a senha corretamente")
            }
        }
    }

    override fun erroMessage() {
        showToast("falha na requisição")

        runOnUiThread {
            load.visibility = View.GONE
        }
    }

    fun validarLicenca(){
        if (application.setBaseUrl()){
            load.visibility = View.VISIBLE
            presenter.validarLicenca(this)
        }
    }

    override fun success(response: Response<ResponseDefault>?) {
        runOnUiThread{
            load.visibility = View.GONE
        }

        if (!response?.body()?.mensagem!!.isEmpty()){
            showToast(response?.body()?.mensagem!!)
        } else {
            openTable()
        }
    }
}