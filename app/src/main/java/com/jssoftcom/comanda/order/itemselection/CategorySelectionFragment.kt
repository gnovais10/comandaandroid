package com.gustavon.comandaandroid.itemselection

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment.keys.NOME
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment.keys.NRCARTAO
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment.keys.NRMESA
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.home.HomeActivity
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.itemselection.ItemSelectionContract
import com.jssoftcom.comanda.order.itemselection.ItemSelectionPresenter
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.CategoryAdapter
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract
import com.jssoftcom.comanda.ordersummary.OrderSummaryFragment
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.selection_fragment.*
import retrofit2.Response


class CategorySelectionFragment : Fragment(), ItemSelectionContract.View, OrderListenerContract {
    val categorias = ArrayList<Item>()
    var selectedItems: ArrayList<Item> = arrayListOf()

    lateinit var nome: String
    lateinit var nrMesa: String
    lateinit var nrCartao: String

    lateinit var presenter: ItemSelectionPresenter

    override fun startPresenter() {
        presenter = ItemSelectionPresenter()
        presenter.start()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.selection_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getInfoTable()

        if (categorias.size == 0) {
            prepareList()
        }

        configLeftButton()
        configureConfirmItemSelection()
    }

    private fun configureConfirmItemSelection() {
        confirmItemSelection.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)


            var itemSelectionFragment = OrderSummaryFragment()
            itemSelectionFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, itemSelectionFragment, "item_selection")
                    .addToBackStack("item_selection").commit()

            for (item in selectedItems) {
                if (activity.application.setBaseUrl()) {
                    if (item.enviado.equals("n")){
                        item.enviado = "s"
                        var idGarcom = ParametersPreference.instance.getIdGarcom(this.activity, ParametersPreference.ID_GARCOM)

                        presenter.addProdutos(this, item.idProduto, nrMesa, nrCartao, nome, item.quantity.toString(), idGarcom, item.obsItem, item.idsOpcao)
                    }
                }
            }
        }
    }

    fun getInfoTable() {
        val bundle = this.arguments
        if (bundle != null) {
            nome = bundle.getString(NOME, "")
            nrMesa = bundle.getString(NRMESA, "")
            nrCartao = bundle.getString(NRCARTAO, "")
            if (arguments.getParcelableArrayList<Item>("SELECTED_ITEMS") != null){
                selectedItems = arguments.getParcelableArrayList<Item>("SELECTED_ITEMS")

                if (selectedItems.size > 0){
                    confirmItemSelection.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun configList(categorias: ArrayList<Categorias>) {
        mainItemRecyclerView.layoutManager = LinearLayoutManager(context)
        mainItemRecyclerView.adapter = CategoryAdapter(categorias, nome, nrMesa, nrCartao, selectedItems)

        load.visibility = View.GONE
    }

    fun configLeftButton() {
        leftIcon.setOnClickListener {
            var alert: AlertDialog

            val builder = AlertDialog.Builder(this.activity)
            builder.setView(R.layout.cancelar_produto_dialog)

            alert = builder.create()
            alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alert.show()

            var btnRight = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_right)
            btnRight.setOnClickListener {
                startActivity(Intent(this.activity, HomeActivity::class.java))
            }

            var btnLeft = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_left)
            btnLeft.setOnClickListener {
                alert.dismiss()
            }
        }
    }


    fun prepareList() {
        load.visibility = View.VISIBLE
        ComandaApplication.database.categoriasDao().getCategorias()
                .subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { values ->
                    Log.i("QTDE DE CATEGORIAS", values.size.toString())

                    configList(values as ArrayList<Categorias>)
                }
    }

    override fun success(settingsResponse: Response<ResponseDefault>) {
    }

    override fun erroMessage() {
    }
}