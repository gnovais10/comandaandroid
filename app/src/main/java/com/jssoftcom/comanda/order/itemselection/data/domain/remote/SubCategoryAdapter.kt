package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.database.tables.SubCategorias
import com.jssoftcom.comanda.model.Item
import kotlinx.android.synthetic.main.sub_category_item.view.*

/**
 * Created by gustavon on 16/01/18.
 */
class SubCategoryAdapter(var subCategorias: ArrayList<SubCategorias>, var idCategoria : String, var nome: String, var nrMesa: String, var nrCartao: String, var selectedItems : ArrayList<Item> ): RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {

    lateinit var activity : FragmentActivity

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.sub_category_item, parent, false)
        activity = parent?.context as FragmentActivity
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.groupDescription?.text = subCategorias.get(position).Descricao

        holder?.layoutSubCategory?.setOnClickListener{
            val bundle = Bundle()
            bundle.putString("id_categoria", idCategoria)
            bundle.putString("id_sub_categoria", subCategorias.get(position).IDSubCategoria)
            bundle.putString(OpenTableFragment.NOME, nome)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)


            var productSelectFragment = ProductSelectFragment()
            productSelectFragment.arguments = bundle


            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, productSelectFragment, "product_select")
                    .addToBackStack("product_select").commit()
        }
    }

    override fun getItemCount(): Int {
        return subCategorias.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val groupDescription = itemView.groupDescription
        val layoutSubCategory = itemView.layoutSubCategory
    }
}