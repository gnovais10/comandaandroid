package com.jssoftcom.comanda.order

import com.jssoftcom.comanda.order.domain.data.remote.OpenTableListenerContract
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract

/**
 * Created by gustavon on 12/11/17.
 */

interface OpenTableContract {

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun abreMesa(openTableListenerContract: OpenTableListenerContract, nrMesa: String, nrCartao: String, nome : String)
        fun identificaGarcom(identificaGarcomListenerContract: OpenTableListenerContract.identificaGarcomListenerContract, senhaGarcom: String)
        fun validarLicenca(validarLicencaListenerContract: ParametersListenersContract.validarLicencaListenerContract)
        fun start()
    }
}
