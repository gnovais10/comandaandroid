package com.jssoftcom.comanda.order.domain.data.remote

import com.google.gson.annotations.SerializedName

/**
 * Created by gustavon on 12/11/17.
 */

class ResponseDefault {

    @SerializedName("Retorno")
    lateinit var retorno : String

    @SerializedName("Mensagem")
    lateinit var mensagem : String

}