package com.example.jssoftcom.comanda.order.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import com.gustavon.comandaandroid.itemselection.CategorySelectionFragment
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.order.OpenTableContract
import com.jssoftcom.comanda.order.OpenTablePresenter
import com.jssoftcom.comanda.order.domain.data.remote.OpenTableListenerContract
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import kotlinx.android.synthetic.main.open_table_fragment.*
import retrofit2.Response


/**
 * Created by gustavon on 29/10/17.
 */
class OpenTableFragment : Fragment(), OpenTableListenerContract, OpenTableContract.View{

    lateinit var presenter : OpenTableContract.Presenter

    companion object keys {
        const val NOME = "nome"
        const val NRMESA = "nrmesa"
        const val NRCARTAO = "nrCartao"
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.open_table_fragment, container, false)
    }

    override fun startPresenter() {
     presenter = OpenTablePresenter()
     presenter.start()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configClose()
        configLogout()
        configTableOpening()
        startPresenter()
    }

    lateinit var nome : String
    lateinit var nrmesa : String
    lateinit var nrCartao : String

    fun configTableOpening() {
        confirmTableOpening.setOnClickListener {

            if (activity.application.setBaseUrl()){
                nome = edtNome.text.toString()
                nrmesa = edtNumeroMesa.text.toString()
                nrCartao = edtNumeroCartao.text.toString()

                if (nrmesa.isEmpty()){
                    (activity as BaseActivity).showToast("Preencher o número da mesa")
                } else  {
                    presenter.abreMesa(this, nrmesa, nrCartao, nome);
                }
            }
        }
    }

    fun configClose() {
        closeButton.setOnClickListener {
            activity.onBackPressed()
        }
    }

    fun configLogout(){
        var identificaGarcom = ParametersPreference.instance.getIdentificaGarcom(this.activity, ParametersPreference.IDENTIFICA_GARCOM)

        if (identificaGarcom){
            logoutButton.visibility = View.VISIBLE
        }

        logoutButton.setOnClickListener{
            ParametersPreference.instance.saveIdGarcom(this.activity, ParametersPreference.ID_GARCOM, "")
            ParametersPreference.instance.garcomLogado(this.activity, ParametersPreference.GARCOM_LOGADO, false)
            activity.onBackPressed()
        }
    }

    override fun success(settingsResponseDefault: Response<ResponseDefault>) {

        if (settingsResponseDefault.body().mensagem.isEmpty()){
            if (settingsResponseDefault.body().retorno.equals("2")){

                activity.runOnUiThread{
                    var alert: AlertDialog

                    val builder = AlertDialog.Builder(this.activity)
                    builder.setView(R.layout.dialog_table)

                    alert = builder.create()
                    alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    alert.show()

                    var btnRight = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_right)
                    btnRight.setOnClickListener {
                        alert.dismiss()
                        changeNextFragment()
                    }

                    var btnLeft = alert.window.decorView.findViewById<Button>(R.id.custom_dialog_button_left)
                    btnLeft.setOnClickListener {
                        alert.dismiss()
                        activity.onBackPressed()
                    }
                }
            } else {
                changeNextFragment()
            }
        } else {
            (activity as BaseActivity).showToast(settingsResponseDefault.body().mensagem)
        }
    }

    private fun changeNextFragment() {
        val bundle = Bundle()
        bundle.putString(NOME, nome)
        bundle.putString(NRMESA, nrmesa)

        if (nrCartao.isEmpty()){
            bundle.putString(NRCARTAO, "0")
        } else {
            bundle.putString(NRCARTAO, nrCartao)
        }

        var itemSelectionFragment = CategorySelectionFragment()
        itemSelectionFragment.arguments = bundle

        activity.supportFragmentManager.beginTransaction()
                .replace(R.id.orderContainer, itemSelectionFragment, "item_selection")
                .addToBackStack("item_selection").commit()
    }

    override fun erroMessage() {
        (activity as BaseActivity).showToast("Falha na requisição")
    }


}