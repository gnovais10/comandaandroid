package com.jssoftcom.comanda.order.domain.data.remote

import com.example.jssoftcom.comanda.infra.ComandaAndroidAPI
import com.jssoftcom.comanda.infra.RetrofitProvider
import com.jssoftcom.comanda.infra.handleError.MyCallBack
import retrofit2.Response
import java.io.IOException


/**
 * Created by gustavon on 12/11/17.
 */
class PreAccountRemoteRepository {

    fun visualizaPreConta(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        val data = hashMapOf<String, String>()
        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        data.put("oNroPessoas", "1")
        data.put("oServico", "1")

        remoteRepository.visualizaPreConta(data).enqueue(object : MyCallBack<List<ResponsePreConta>> {
            override fun success(responseDefault: Response<List<ResponsePreConta>>) {
                listener.success(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }

    fun emitirPreConta(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        val data = hashMapOf<String, String>()
        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        data.put("oNroPessoas", "1")
        data.put("oServico", "1")

        remoteRepository.emitirPreConta(data).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(responseDefault: Response<ResponseDefault>) {
                listener.successPre(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }
}