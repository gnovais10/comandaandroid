package com.jssoftcom.comanda.order.domain.data.remote

import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract

/**
 * Created by gustavon on 12/11/17.
 */
class OpenTableRepository(var openTableRemoteRepository: OpenTableRemoteRepository) {

    fun abreMesa(listenerContract: OpenTableListenerContract, nrMesa: String, nrCartao: String, nome: String) {
        openTableRemoteRepository.abreMesa(listenerContract, nrMesa, nrCartao, nome)
    }

    fun identificaGarcom(identificaGarcomListenerContract: OpenTableListenerContract.identificaGarcomListenerContract, senhaGarcom: String) {
        openTableRemoteRepository.identificaGarcom(identificaGarcomListenerContract, senhaGarcom)
    }

    fun validarLicenca(validarLicencaListenerContract: ParametersListenersContract.validarLicencaListenerContract) {
        openTableRemoteRepository.validaLicenca(validarLicencaListenerContract)
    }

}