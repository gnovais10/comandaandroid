package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import retrofit2.Response

/**
 * Created by gustavon on 12/11/17.
 */
interface OrderDeleteItemListenerContract {
    fun successDeleteItem(settingsResponse: Response<ResponseDefault>)

    fun erroMessage()
}