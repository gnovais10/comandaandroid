package com.jssoftcom.comanda.order

import com.jssoftcom.comanda.order.domain.data.remote.OpenTableListenerContract
import com.jssoftcom.comanda.order.domain.data.remote.PreAccountListenerContract

/**
 * Created by gustavon on 12/11/17.
 */

interface PreAccountContract {

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun preAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String)
        fun emitirPreAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String)
        fun start()
    }
}
