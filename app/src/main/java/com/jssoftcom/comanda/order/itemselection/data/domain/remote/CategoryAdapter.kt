package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.database.tables.Categorias
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.itemselection.SubCategoryFragment
import kotlinx.android.synthetic.main.selection_main_item.view.*

/**
 * Created by gustavon on 16/01/18.
 */
class CategoryAdapter (var categorias: ArrayList<Categorias>, var nome: String, var nrMesa: String, var nrCartao: String, var selectedItems : ArrayList<Item> ): RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    lateinit var activity : FragmentActivity

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.selection_main_item, parent, false)
        activity = parent?.context as FragmentActivity
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.mainItemDescription?.text = categorias.get(position).Descricao

        holder?.layoutCategoria?.setOnClickListener{
            val bundle = Bundle()
            bundle.putString("id_categoria", categorias.get(position).IDCategoria)
            bundle.putString(OpenTableFragment.NOME, nome)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)

            var subCategory = SubCategoryFragment()
            subCategory.arguments = bundle


            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, subCategory, "sub_category")
                    .addToBackStack("sub_category").commit()
        }
    }

    override fun getItemCount(): Int {
        return categorias.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mainItemDescription = itemView.mainItemDescription
        val layoutCategoria = itemView.layoutCategoria
    }
}