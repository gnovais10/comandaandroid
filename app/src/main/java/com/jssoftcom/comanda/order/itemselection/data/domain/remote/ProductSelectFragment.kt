package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.ObsOrderActivity
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.gustavon.comandaandroid.itemselection.CategorySelectionFragment
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.itemselection.ItemSelectionContract
import com.jssoftcom.comanda.order.itemselection.ItemSelectionPresenter
import com.jssoftcom.comanda.ordersummary.OrderSummaryFragment
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.product_fragment.*
import retrofit2.Response

/**
 * Created by gustavon on 16/01/18.
 */
class ProductSelectFragment : Fragment () ,  ProductAdapter.OnItemSelect, ProductAdapter.OnQuantityChange, ProductAdapter.itemObsClick, OrderListenerContract,  ItemSelectionContract.View {
    lateinit var categoriaId : String
    lateinit var subCategoriaId : String

    var selectedItems: ArrayList<Item> = arrayListOf()
    val categorias = ArrayList<Item>()
    lateinit var nome: String
    lateinit var nrMesa: String
    lateinit var nrCartao: String

    lateinit var presenter: ItemSelectionPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (savedInstanceState != null) {
            selectedItems = savedInstanceState.getParcelableArrayList("SELECTED_ITEMS")
        }
        getInfo()
        return inflater?.inflate(R.layout.product_fragment, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putParcelableArrayList("SELECTED_ITEMS", selectedItems)
    }

    fun getInfo() {
        val bundle = this.arguments
        if (bundle != null) {
            categoriaId = bundle.getString("id_categoria", "")
            subCategoriaId = bundle.getString("id_sub_categoria", "")

            if (arguments.getParcelableArrayList<Item>("SELECTED_ITEMS") != null){
                selectedItems = arguments.getParcelableArrayList<Item>("SELECTED_ITEMS")
            }
        }
    }

    fun getInfoTable() {
        val bundle = this.arguments
        if (bundle != null) {
            nome = bundle.getString(OpenTableFragment.NOME, "")
            nrMesa = bundle.getString(OpenTableFragment.NRMESA, "")
            nrCartao = bundle.getString(OpenTableFragment.NRCARTAO, "")
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startPresenter()
        getInfo()
        getInfoTable()
        configureConfirmItemSelection()
        configureConfirmAddMore()

        // pega todos produtos da categoria e subcategoria
        ComandaApplication.database.produtosDao().getProdutosBySubCategoria(subCategoriaId, categoriaId)
        //ComandaApplication.database.produtosDao().getProdutosBySubCategoria(subCategoriaId, categoriaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { values ->
                    recyclerViewProduct.layoutManager = LinearLayoutManager(context)

                    var produtos = ArrayList<Item>()
                    for (produto in values){
                        produtos.add(Item(idProduto = produto.IDProduto, description = produto.Descricao, value = produto.Preco, idCategoria = categoriaId))
                    }

                    recyclerViewProduct.adapter = ProductAdapter(produtos, this, this, this)
                }

    }

    private fun configureConfirmAddMore() {
        confirmAddMore.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)


            var itemSelectionFragment = CategorySelectionFragment()
            itemSelectionFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, itemSelectionFragment, "item_selection")
                    .addToBackStack("item_selection").commit()

            for (item in selectedItems) {
                if (activity.application.setBaseUrl()) {
                    if (item.enviado.equals("n")){
                        item.enviado = "s"
                        var idGarcom = ParametersPreference.instance.getIdGarcom(this.activity, ParametersPreference.ID_GARCOM)

                        presenter.addProdutos(this, item.idProduto, nrMesa, nrCartao, nome, item.quantity.toString(), idGarcom, item.obsItem, item.idsOpcao)
                    }
                }
            }
        }
    }

    private fun configureConfirmItemSelection() {
        confirmItemSelection.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)


            var itemSelectionFragment = OrderSummaryFragment()
            itemSelectionFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, itemSelectionFragment, "item_selection")
                    .addToBackStack("item_selection").commit()

            for (item in selectedItems) {
                if (activity.application.setBaseUrl()) {
                    if (item.enviado.equals("n")){
                        item.enviado = "s"
                        var idGarcom = ParametersPreference.instance.getIdGarcom(this.activity, ParametersPreference.ID_GARCOM)

                        presenter.addProdutos(this, item.idProduto, nrMesa, nrCartao, nome, item.quantity.toString(), idGarcom, item.obsItem, item.idsOpcao)
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //adiciona as opcoes selecionadas na lista de items
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                var opcoes = data?.extras?.getString("opcoes")
                var listOpcoes = data?.getParcelableArrayListExtra<SettingsResponse.Opcao>("listOpcoes") as ArrayList<SettingsResponse.Opcao>


                var itemId = data?.extras?.getString("idItem")
                var obs = data?.extras?.getString("obs") as String

                for (item in selectedItems){
                    if (item.idProduto.equals(itemId.toString())){
                        item.idsOpcao = "[${opcoes}]"
                        item.obsItem = obs;
                        item.opcoes = listOpcoes
                    }
                }
            }
        }
    }
    override fun itemObsClick(item: Item) {
        var intent  = Intent(this.context, ObsOrderActivity::class.java)
        intent.putExtra("idItem", item?.idProduto)
        intent.putExtra("descItem", item?.description)
        intent.putExtra("idCategoria", item?.idCategoria)
        (this.context as BaseActivity).startActivityForResult(intent, 0)
    }

    override fun itemSelected(item: Item?) {
        if (selectedItems.contains(item)) {
            //selectedItems.remove(item)
            if (item!!.quantity <= 0) {
                confirmItemSelection.visibility = View.GONE
                confirmAddMore.visibility = View.GONE
            }
        } else {
            selectedItems.add(item!!)
        }

        if (item!!.quantity >= 1) {
            confirmItemSelection.visibility = View.VISIBLE
            confirmAddMore.visibility = View.VISIBLE
        }
    }

    override fun itemQuantityChange(item: Item?) {
        confirmItemSelection.visibility = View.VISIBLE
        confirmAddMore.visibility = View.VISIBLE

        if (selectedItems.contains(item)) {
            //selectedItems.remove(item)

            selectedItems.add(item!!)

            if (item!!.quantity > 0) {
                confirmItemSelection.visibility = View.VISIBLE
                confirmAddMore.visibility = View.VISIBLE
            } else if (item!!.quantity <= 0) {
                confirmItemSelection.visibility = View.GONE
                confirmAddMore.visibility = View.GONE

                selectedItems.forEach {
                    if (it.quantity == 0){
                        confirmItemSelection.visibility = View.GONE
                        confirmAddMore.visibility = View.GONE
                    } else {
                        confirmItemSelection.visibility = View.VISIBLE
                        confirmAddMore.visibility = View.VISIBLE
                    }
                }
            }
        } else {
            if (item!!.quantity == 1){
                selectedItems.forEach {
                    if (it.equals(item)){
                        item!!.quantity = 1
                    }
                }
            } else {
                selectedItems.add(item!!)
            }

        }
    }

    override fun startPresenter() {
        presenter = ItemSelectionPresenter()
        presenter.start()
    }

    override fun success(settingsResponse: Response<ResponseDefault>) {
    }

    override fun erroMessage() {}
}