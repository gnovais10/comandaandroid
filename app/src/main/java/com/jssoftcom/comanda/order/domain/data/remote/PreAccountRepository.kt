package com.jssoftcom.comanda.order.domain.data.remote

/**
 * Created by gustavon on 28/11/17.
 */
class PreAccountRepository(var preAccountRemoteRepository: PreAccountRemoteRepository) {

    fun preAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String) {
        preAccountRemoteRepository.visualizaPreConta(listener, nrMesa, nrCartao)
    }

    fun emitirPreAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String) {
        preAccountRemoteRepository.emitirPreConta(listener, nrMesa, nrCartao)
    }
}