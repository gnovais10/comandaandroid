package com.jssoftcom.comanda.order.domain.data.remote;

import java.util.List;

/**
 * Created by gustavon on 28/11/17.
 */

public class ResponsePreConta {

    private List<PreConta> PreConta;
    private List<Totais> Totais;

    public List<PreConta> getPreConta() {
        return PreConta;
    }

    public void setPreConta(List<PreConta> PreConta) {
        this.PreConta = PreConta;
    }

    public List<Totais> getTotais() {
        return Totais;
    }

    public void setTotais(List<Totais> Totais) {
        this.Totais = Totais;
    }

    public static class PreConta {
        /**
         * Qtde : 1,00
         * Produto : X-SALADA ESPECIAL
         * Total : 24,90
         */

        private String Qtde;
        private String Produto;
        private String Total;

        public String getQtde() {
            return Qtde;
        }

        public void setQtde(String Qtde) {
            this.Qtde = Qtde;
        }

        public String getProduto() {
            return Produto;
        }

        public void setProduto(String Produto) {
            this.Produto = Produto;
        }

        public String getTotal() {
            return Total;
        }

        public void setTotal(String Total) {
            this.Total = Total;
        }
    }

    public static class Totais {
        /**
         * TotalProdutos : 60,2
         * Servico : 6,02
         * TotalConta : 66,22
         * TotalPorPessoa : 66
         */

        private String TotalProdutos;
        private String Servico;
        private String TotalConta;
        private String TotalPorPessoa;

        public String getTotalProdutos() {
            return TotalProdutos;
        }

        public void setTotalProdutos(String TotalProdutos) {
            this.TotalProdutos = TotalProdutos;
        }

        public String getServico() {
            return Servico;
        }

        public void setServico(String Servico) {
            this.Servico = Servico;
        }

        public String getTotalConta() {
            return TotalConta;
        }

        public void setTotalConta(String TotalConta) {
            this.TotalConta = TotalConta;
        }

        public String getTotalPorPessoa() {
            return TotalPorPessoa;
        }

        public void setTotalPorPessoa(String TotalPorPessoa) {
            this.TotalPorPessoa = TotalPorPessoa;
        }
    }
}
