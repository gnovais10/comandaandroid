package com.jssoftcom.comanda.order.domain.data.remote

import com.example.jssoftcom.comanda.infra.ComandaAndroidAPI
import com.jssoftcom.comanda.BuildConfig
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.infra.RetrofitProvider
import com.jssoftcom.comanda.infra.handleError.MyCallBack
import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract
import retrofit2.Response
import java.io.IOException
import java.util.LinkedHashMap


/**
 * Created by gustavon on 12/11/17.
 */
class OpenTableRemoteRepository {

    fun abreMesa(listener: OpenTableListenerContract, nrMesa: String, nrCartao: String, nome : String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        val data = hashMapOf<String, String>()
        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        if (!nome.isEmpty()){
            data.put("oNome", nome)
        }

        data.put("oIDDispositivo", ComandaApplication.nomeDispositivo)
        data.put("vendorID", ComandaApplication.vendorId)

        remoteRepository.abreMesa(data).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(responseDefault: Response<ResponseDefault>) {
                listener.success(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }

    fun identificaGarcom(listener: OpenTableListenerContract.identificaGarcomListenerContract, senhaGarcom: String) {
        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        val data = LinkedHashMap<String, String>()
        data.put("asenha", senhaGarcom)

        remoteRepository.identificaGarcom(data).enqueue(object : MyCallBack<IdentifcaGarcomResponse> {
            override fun success(response: Response<IdentifcaGarcomResponse>) {
                listener.successIdentifcaGarcom(response)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })
    }

    fun validaLicenca(listener: ParametersListenersContract.validarLicencaListenerContract) {
        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)

        val map = LinkedHashMap<String, String>()
        map.put("vendorID", ComandaApplication.vendorId)
        map.put("Versao", BuildConfig.VERSION_NAME)


        remoteRepository.validarLicencas(map).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(response: Response<ResponseDefault>) {
                listener.success(response)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })
    }
}