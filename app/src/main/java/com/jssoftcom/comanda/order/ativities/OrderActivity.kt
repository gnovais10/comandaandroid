package com.jssoftcom.comanda.order.ativities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.ProductSelectFragment
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse

class OrderActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_order)


        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.orderContainer, OpenTableFragment())
        transaction.commitAllowingStateLoss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                supportFragmentManager.fragments.forEach {
                    if (it is ProductSelectFragment){
                        it.onActivityResult(requestCode, resultCode, data)
                    }
                }
            }
        }
    }
}
