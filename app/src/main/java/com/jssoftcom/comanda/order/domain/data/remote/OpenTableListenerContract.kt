package com.jssoftcom.comanda.order.domain.data.remote

import com.jssoftcom.comanda.settings.parameters.IdentifcaGarcomResponse
import retrofit2.Response

/**
 * Created by gustavon on 12/11/17.
 */
interface OpenTableListenerContract{
    fun success(settingsResponseDefault: Response<ResponseDefault>)

    fun erroMessage()

    interface identificaGarcomListenerContract {
        fun successIdentifcaGarcom(response: Response<IdentifcaGarcomResponse>)
        fun erroMessage()
    }
}