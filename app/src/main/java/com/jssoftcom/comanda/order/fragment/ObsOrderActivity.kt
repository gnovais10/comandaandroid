package com.example.jssoftcom.comanda.order.fragment


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.ordersummary.adapter.ObsAdapter
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.obs_order_fragment.*


/**
 * Created by gustavon on 29/10/17.
 */
class ObsOrderActivity : BaseActivity() {

    lateinit var itemId : String
    lateinit var idCategoria : String
    lateinit var descItem : String
    lateinit var listItemOpcao : ArrayList<SettingsResponse.Opcao>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.obs_order_fragment)

        ObsAdapter.opcoesSelecionadas = mutableListOf()
        ObsAdapter.opcoesSelecionadasObj = mutableListOf()

        listItemOpcao = arrayListOf<SettingsResponse.Opcao>()

        itemId = intent.extras.getString("idItem")
        idCategoria = intent.extras.getString("idCategoria")
        descItem = intent.extras.getString("descItem")

        ComandaApplication.database.opcoesDao().getOpcoesByCategoria(idCategoria)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe { values ->
                    Log.i("QTDE DE OPCOES", values.size.toString())

                    for (item in values) {
                        var opcao = SettingsResponse.Opcao()
                        opcao.descricao = item.Descricao
                        opcao.idCategoria = item.IDCategoria
                        opcao.idOpcao = item.IDOpcao
                        opcao.ordem = item.Ordem

                        listItemOpcao.add(opcao)

                        if (values.last().equals(item)) {
                            recyclerViewItemOpcao.adapter = (ObsAdapter(listItemOpcao))
                            recyclerViewItemOpcao.layoutManager = LinearLayoutManager(this)
                        }
                    }
                }

        configClose()

        textDesc.text = descItem;
    }

    fun configClose(){
        close.setOnClickListener{
            createResultIntent()
        }
    }

    override fun onBackPressed() {
        createResultIntent()
    }

    private fun createResultIntent() {
        val intent = Intent()
        var opcoes = String()

        for (item in ObsAdapter.opcoesSelecionadas) {
            if (item.equals(ObsAdapter.opcoesSelecionadas.last())) {
                opcoes += "${item}"
            } else {
                opcoes += "${item},"
            }
        }

        intent.putExtra("opcoes", opcoes)
        intent.putExtra("idItem", itemId)
        intent.putExtra("obs", edtObs.text.toString())
        intent.putParcelableArrayListExtra("listOpcoes", ObsAdapter.opcoesSelecionadasObj as ArrayList<SettingsResponse.Opcao>)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


}