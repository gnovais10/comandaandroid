package com.jssoftcom.comanda.order.ativities

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.home.HomeActivity
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.order.PreAccountAdapter
import com.jssoftcom.comanda.order.PreAccountContract
import com.jssoftcom.comanda.order.PreAccountPresenter
import com.jssoftcom.comanda.order.ativities.SelectTablePreAccountActivity.keys.NRCARTAO
import com.jssoftcom.comanda.order.ativities.SelectTablePreAccountActivity.keys.NRMESA
import com.jssoftcom.comanda.order.domain.data.remote.PreAccountListenerContract
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.domain.data.remote.ResponsePreConta
import kotlinx.android.synthetic.main.obs_order_fragment.*
import kotlinx.android.synthetic.main.pre_account_fragment.*
import retrofit2.Response


/**
 * Created by gustavon on 29/10/17.
 */
class PreAccountActity : BaseActivity(), PreAccountContract.View, PreAccountListenerContract{

    lateinit var preAccountPresenter : PreAccountPresenter

    lateinit var nrmesa : String
    lateinit var nrCartao : String


    var adapter = PreAccountAdapter(arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pre_account_fragment)

        startPresenter()
        configClose()

        val bundle = intent.extras
        if (bundle != null)
            nrCartao = bundle.getString(NRCARTAO)
            nrmesa = bundle.getString(NRMESA)

        setBaseUrl()

        load.visibility = View.VISIBLE
        preAccountPresenter.preAccount(this, nrmesa, nrCartao)

        confirmPreAccount()
    }

    override fun startPresenter() {
        preAccountPresenter = PreAccountPresenter()
        preAccountPresenter.start()
    }

    override fun success(settingsResponseDefault: Response<List<ResponsePreConta>>) {
        runOnUiThread{
            preAccountRecycler.layoutManager = LinearLayoutManager(this)

            var itens =  settingsResponseDefault.body().get(0).preConta as ArrayList<ResponsePreConta.PreConta>

            adapter = PreAccountAdapter(itens)
            preAccountRecycler.adapter = adapter
            adapter.notifyDataSetChanged()

            totalValue.text = settingsResponseDefault.body().get(1).totais.get(0).totalConta
            load.visibility = View.GONE
        }
    }

    override fun successPre(settingsResponseDefault: Response<ResponseDefault>) {
        runOnUiThread {
            showToast("pre conta emitida com sucesso!")
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }

    fun confirmPreAccount(){
        relativeConfimPreAccount.setOnClickListener{
            preAccountPresenter.emitirPreAccount(this, nrmesa, nrCartao)
        }
    }

    override fun erroMessage() {
        runOnUiThread{
            load.visibility = View.GONE
            showToast("informe a mesa corretamente")
            onBackPressed()
        }
    }

    fun configClose() {
        closeButton.setOnClickListener {
            onBackPressed()
        }
    }
}