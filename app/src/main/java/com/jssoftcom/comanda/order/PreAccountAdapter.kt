package com.jssoftcom.comanda.order

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.domain.data.remote.ResponsePreConta
import kotlinx.android.synthetic.main.order_summary_item.view.*

class PreAccountAdapter(items: ArrayList<ResponsePreConta.PreConta>): RecyclerView.Adapter<PreAccountAdapter.OrderSummaryViewHolder>() {

    var orderSummaryItems: ArrayList<ResponsePreConta.PreConta> = items

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OrderSummaryViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.order_summary_item, parent, false)
        return OrderSummaryViewHolder(view)
    }

    override fun onBindViewHolder(holder: OrderSummaryViewHolder?, position: Int) {
        val item = orderSummaryItems[position]

        var qtde = item.qtde.replace(".",",").substringBefore(",")

        holder?.itemQuantity?.setText("${qtde}")
        holder?.itemDescription?.setText(item.produto)

        if (qtde.toInt() > 1){
            var valor : Float = item.total.replace(",",".").toFloat()// * qtde.toInt()
            holder?.itemValue?.setText(valor.toString().replace(".","."))
        } else {
            holder?.itemValue?.setText(item.total)
        }

    }

    override fun getItemCount(): Int {
        return orderSummaryItems.size
    }


    class OrderSummaryViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val itemQuantity = view.itemQuantity
        val itemDescription = view.itemDescription
        val itemValue = view.itemValue
        val viewForeground = view.viewForeground

    }

    fun removeItem(position: Int) {
        orderSummaryItems.removeAt(position)
        notifyItemRemoved(position)
    }
}