package com.jssoftcom.comanda.order

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.home.HomeActivity
import kotlinx.android.synthetic.main.order_closed_fragment.*

/**
 * Created by gustavon on 16/11/17.
 */
class OrderClosedFragment : Fragment(){


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.order_closed_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        order_closed.setOnClickListener{

            var intent = Intent((this.activity), HomeActivity::class.java)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

            activity.startActivity(intent)
        }
    }

}