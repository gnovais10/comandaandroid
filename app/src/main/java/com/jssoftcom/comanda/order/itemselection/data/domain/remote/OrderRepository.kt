package com.jssoftcom.comanda.order.itemselection.data.domain.remote

/**
 * Created by gustavon on 12/11/17.
 */
class OrderRepository(var orderRemoteRepository: OrderRemoteRepository) {

    fun addProduto(listenerContract: OrderListenerContract, idProduto : String, nrMesa: String, nrCartao: String, nome: String, qtde : String, idGarcom : String , aObservacaoItem : String, idOpcao : String) {
        orderRemoteRepository.addProduto(listenerContract, idProduto, nrMesa, nrCartao, nome, qtde, idGarcom, aObservacaoItem, idOpcao)
    }

    fun finalizarPedido(listenerContract: OrderListenerContract, nrMesa: String, nrCartao: String) {
        orderRemoteRepository.finalizarPedido(listenerContract, nrMesa, nrCartao)
    }

    fun deletarItem(listenerContract: OrderDeleteItemListenerContract, nrMesa: String, nrCartao: String, idProduto: String) {
        orderRemoteRepository.deletarItem(listenerContract, nrMesa, nrCartao, idProduto)
    }
}