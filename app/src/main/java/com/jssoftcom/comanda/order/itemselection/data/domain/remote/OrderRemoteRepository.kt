package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import com.example.jssoftcom.comanda.infra.ComandaAndroidAPI
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.infra.RetrofitProvider
import com.jssoftcom.comanda.infra.handleError.MyCallBack
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import retrofit2.Response
import java.io.IOException


/**
 * Created by gustavon on 12/11/17.
 */
class OrderRemoteRepository {

    fun addProduto(listener: OrderListenerContract, idProduto : String, nrMesa: String, nrCartao: String, nome : String, qtde : String, oIDGarcom : String, aObservacaoItem : String, idOpcao : String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)
        val data = LinkedHashMap<String, String>()

        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        if (!nome.isEmpty()){
            data.put("oNome", nome)
        }

        if (!idOpcao.isEmpty() && !idOpcao.equals("[]")){
            data.put("aOpcao", idOpcao)
        } else {
            data.put("aOpcao", "0")
        }

        data.put("oidProduto", idProduto)
        data.put("aQtde", qtde)
        data.put("aObservacaoItem", aObservacaoItem)
        data.put("oIDGarcom", oIDGarcom)
        data.put("oIDDispositivo", ComandaApplication.nomeDispositivo)
        data.put("vendorID", ComandaApplication.vendorId)

        remoteRepository.addProduto(data).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(responseDefault: Response<ResponseDefault>) {
                listener.success(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }

    fun finalizarPedido(listener: OrderListenerContract, nrMesa: String, nrCartao : String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)
        val data = LinkedHashMap<String, String>()

        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        remoteRepository.finalizarPedido(data).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(responseDefault: Response<ResponseDefault>) {
                listener.success(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }

    fun deletarItem(listener: OrderDeleteItemListenerContract, nrMesa: String, nrCartao : String, idProduto: String){

        var remoteRepository = RetrofitProvider.retrofit.create(ComandaAndroidAPI::class.java)
        val data = LinkedHashMap<String, String>()

        data.put("oNroMesa", nrMesa)

        if (!nrCartao.isEmpty()){
            data.put("oNroCartao", nrCartao)
        } else {
            data.put("oNroCartao", "0")
        }

        data.put("oIDProduto", idProduto)

        remoteRepository.removeProduto(data).enqueue(object : MyCallBack<ResponseDefault> {
            override fun success(responseDefault: Response<ResponseDefault>) {
                listener.successDeleteItem(responseDefault)
            }

            override fun unauthenticated(response: Response<*>) {
                listener.erroMessage()
            }

            override fun clientError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun serverError(response: Response<*>) {
                listener.erroMessage()
            }

            override fun networkError(e: IOException) {
                listener.erroMessage()
            }

            override fun unexpectedError(t: Throwable) {
                listener.erroMessage()
            }
        })

    }
}