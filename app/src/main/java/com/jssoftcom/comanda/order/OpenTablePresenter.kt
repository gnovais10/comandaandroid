package com.jssoftcom.comanda.order

import com.jssoftcom.comanda.order.domain.data.remote.OpenTableListenerContract
import com.jssoftcom.comanda.order.domain.data.remote.OpenTableRemoteRepository
import com.jssoftcom.comanda.order.domain.data.remote.OpenTableRepository
import com.jssoftcom.comanda.settings.parameters.domain.data.remote.ParametersListenersContract

/**
 * Created by gustavon on 12/11/17.
 */
class OpenTablePresenter : OpenTableContract.Presenter{

    lateinit var openTableRepository: OpenTableRepository

    override fun start() {
        openTableRepository = OpenTableRepository(OpenTableRemoteRepository())
    }

    override fun abreMesa(openTableListenerContract: OpenTableListenerContract, nrMesa: String, nrCartao: String, nome : String) {
        openTableRepository.abreMesa(openTableListenerContract, nrMesa, nrCartao, nome)
    }

    override fun identificaGarcom(identificaGarcomListenerContract: OpenTableListenerContract.identificaGarcomListenerContract, senhaGarcom: String) {
        openTableRepository.identificaGarcom(identificaGarcomListenerContract, senhaGarcom)
    }

    override fun validarLicenca(validarLicencaListenerContract: ParametersListenersContract.validarLicencaListenerContract) {
        openTableRepository.validarLicenca(validarLicencaListenerContract)
    }
}
