package com.jssoftcom.comanda.order.itemselection.data.domain.remote

import android.content.Intent
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.ObsOrderActivity
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.model.Item
import kotlinx.android.synthetic.main.selection_item.view.*

/**
 * Created by gustavon on 16/01/18.
 */
class ProductAdapter(var products : ArrayList<Item>, var selectListener: OnItemSelect, var quantityChangeListener: OnQuantityChange, var obsListener: itemObsClick):  RecyclerView.Adapter<ProductAdapter.ViewHolder>(), TextWatcher {

    lateinit var activity : FragmentActivity
    lateinit var item : Item

    interface OnItemSelect {
        fun itemSelected(item: Item?)
    }

    interface OnQuantityChange {
        fun itemQuantityChange(item: Item?)
    }

    interface itemObsClick {
        fun itemObsClick(item: Item)
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductAdapter.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.selection_item, parent, false)
        activity = parent?.context as FragmentActivity
        return ProductAdapter.ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductAdapter.ViewHolder?, position: Int) {
        item = products.get(position)
        holder?.itemDescription?.text = item.description

        var itemSelecionado : Boolean = false

        if (item.selecionado.equals("s")){
            itemSelecionado = true
        }

        holder?.itemSelected?.setOnCheckedChangeListener(null)
        holder?.itemSelected?.isChecked = itemSelecionado

        holder?.itemSelected?.setOnCheckedChangeListener { compoundButton, b ->
            holder?.itemSelected?.isSelected  = b

            item = products.get(position)
            holder?.itemQuantity.setText("${item.quantity}")

            item.selecionado = "s"

            refreshList = false
            if (holder?.itemSelected.isSelected) {
                item = item?.copy(quantity = 1)
                holder?.itemQuantity.setText("1")
            } else {
                item = item?.copy(quantity = 0)
                holder?.itemQuantity.setText("0")
            }

            if (invokeCallback) {
                selectListener.itemSelected(item)
            }
        }

        holder?.lessQuantity?.setOnClickListener {
            item = products.get(position)

            refreshList = false
            var quantityBuffer: Int = item!!.quantity.toInt()
            if (quantityBuffer > 0) {
                quantityBuffer = quantityBuffer - 1

                //item = item?.copy(quantity = quantityBuffer)
                this.item.quantity = quantityBuffer

                setItemQuantity(holder, quantityBuffer)
                if (invokeCallback) {
                    quantityChangeListener.itemQuantityChange(item)
                }

                if (quantityBuffer <= 0) {
                    holder?.itemSelected.isChecked = false
                }

            } else {
                holder?.itemSelected.isChecked = false
            }
        }

        holder?.plusQuantity?.setOnClickListener {
            item = products.get(position)

            refreshList = false
            var quantityBuffer: Int = item!!.quantity.toInt()
            if (quantityBuffer < 999) {
                quantityBuffer = quantityBuffer + 1
                //this.item = this.item?.copy(quantity = quantityBuffer)
                this.item.quantity = quantityBuffer

                setItemQuantity(holder, quantityBuffer)
                if (invokeCallback) {
                    quantityChangeListener.itemQuantityChange(item)
                }
                if (!holder?.itemSelected.isChecked) {
                    holder?.itemSelected.isChecked = true
                }
            }
        }

        holder?.moreInformation?.setOnClickListener {
            item = products.get(position)

            if (holder?.itemSelected.isChecked){
                obsListener.itemObsClick(item)
            } else {
                (activity as BaseActivity).showToast("selecione o produto para adicionar opções")
            }

        }

        if(item.selecionado.equals("s")){
            holder?.itemQuantity?.setText(item.quantity.toString())
        } else {
            holder?.itemQuantity?.setText("0")
        }
        holder?.itemQuantity?.addTextChangedListener(this)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemDescription = itemView.itemDescription
        val itemSelected = itemView.itemSelected
        val lessQuantity = itemView.lessQuantity
        val plusQuantity = itemView.plusQuantity
        val moreInformation = itemView.moreInformation
        val itemQuantity = itemView.itemQuantity
    }


    var refreshList = true
    var invokeCallback = true

    fun setItemQuantity(holder : ViewHolder, quantidade: Int) {
        if (holder.itemSelected.isSelected && holder.itemSelected.isChecked){
            holder.itemQuantity?.setText("${quantidade}")
        }
    }

   override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

    }

    override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
        if (charSequence.toString().isBlank()) {
            charSequence.toString().plus("0")
        }
        if (refreshList && item.selecionado == "s" && item.quantity > 0) {
            this.item = this.item?.copy(quantity = charSequence.toString().toInt())
            quantityChangeListener.itemQuantityChange(item)
        }
        refreshList = true
    }

    override fun afterTextChanged(editable: Editable?) {
        /*if (editable.toString().isBlank()) {
            editable?.append("0")
        }
        if (refreshList && item.selecionado == "s" && item.quantity > 0) {
            this.item = this.item?.copy(quantity = editable.toString().toInt())
            quantityChangeListener.itemQuantityChange(item)
        }
        refreshList = true*/
    }

}