package com.jssoftcom.comanda.order

import com.jssoftcom.comanda.order.domain.data.remote.*

/**
 * Created by gustavon on 28/11/17.
 */
class PreAccountPresenter : PreAccountContract.Presenter{

    lateinit var preAccountRepository: PreAccountRepository

    override fun start() {
        preAccountRepository = PreAccountRepository(PreAccountRemoteRepository())
    }

    override fun preAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String) {
        preAccountRepository.preAccount(listener, nrMesa, nrCartao)
    }

    override fun emitirPreAccount(listener: PreAccountListenerContract, nrMesa: String, nrCartao: String) {
        preAccountRepository.emitirPreAccount(listener, nrMesa, nrCartao)
    }
}
