package com.jssoftcom.comanda.order.ativities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.jssoftcom.comanda.BaseActivity
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.infra.setBaseUrl
import kotlinx.android.synthetic.main.select_table_pre_account_activity.*


/**
 * Created by gustavon on 29/10/17.
 */
class SelectTablePreAccountActivity : BaseActivity() {

    companion object keys {
        const val NRMESA = "nrmesa"
        const val NRCARTAO = "nrCartao"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_table_pre_account_activity)

        configClose()
        configTableOpening()
    }

    lateinit var nrmesa : String
    lateinit var nrCartao : String

    fun configTableOpening() {
        confirm.setOnClickListener {

            if (application.setBaseUrl()){
                nrmesa = edtNumeroMesa.text.toString()
                nrCartao = edtNumeroCartao.text.toString()

                if (nrmesa.isEmpty()){
                    showToast("Preencher o número da mesa")
                } else  {
                    val bundle = Bundle()
                    bundle.putString(NRMESA, nrmesa)
                    bundle.putString(NRCARTAO, nrCartao)

                    var intent = Intent(this, PreAccountActity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.putExtras(bundle)

                    startActivity(intent)
                }
            }
        }
    }

    fun configClose() {
        close.setOnClickListener {
            onBackPressed()
        }
    }

}