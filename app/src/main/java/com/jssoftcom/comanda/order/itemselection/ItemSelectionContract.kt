package com.jssoftcom.comanda.order.itemselection

import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract

/**
 * Created by gustavon on 02/11/17.
 */
interface ItemSelectionContract {

    interface View {
        fun startPresenter()
    }

    interface Presenter {
        fun start()
        fun addProdutos(orderListenerContract: OrderListenerContract, idProduto : String, nrMesa: String, nrCartao: String, nome : String, qtde : String, idGarcom : String, aObservacaoItem : String, idOpcao : String)
    }

}