package com.jssoftcom.comanda.order.itemselection

enum class ItemType(val textArgument: String) {
    REGULAR("REGULAR"),
    MAIN("MAIN"),
    GROUP("GROUP");

    val text = textArgument

    companion object {
        fun valueOf(value: String): ItemType {
            when (value) {
                "REGULAR" -> return REGULAR
                "MAIN" -> return MAIN
                else -> {
                    return GROUP
                }
            }
        }
    }
}