package com.jssoftcom.comanda.order.domain.data.remote

import retrofit2.Response

/**
 * Created by gustavon on 12/11/17.
 */
interface PreAccountListenerContract {
    fun success(settingsResponseDefault: Response<List<ResponsePreConta>>)
    fun successPre(settingsResponseDefault: Response<ResponseDefault>)

    fun erroMessage()
}