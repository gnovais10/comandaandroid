package com.jssoftcom.comanda.order.itemselection

import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderRemoteRepository
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderRepository

/**
 * Created by gustavon on 15/11/17.
 */
class ItemSelectionPresenter : ItemSelectionContract.Presenter{

    lateinit var orderRepository: OrderRepository

    override fun start() {
        orderRepository = OrderRepository(OrderRemoteRepository())
    }

    override fun addProdutos(orderContract: OrderListenerContract, idProduto : String, nrMesa: String, nrCartao: String, nome : String, qtde : String, idGarcom : String, aObservacaoItem : String, idOpcao : String) {
       orderRepository.addProduto(orderContract, idProduto, nrMesa, nrCartao, nome, qtde, idGarcom, aObservacaoItem, idOpcao)
    }

}