package com.jssoftcom.comanda.order.itemselection

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.jssoftcom.comanda.order.fragment.OpenTableFragment
import com.jssoftcom.comanda.ComandaApplication
import com.jssoftcom.comanda.R
import com.jssoftcom.comanda.database.tables.SubCategorias
import com.jssoftcom.comanda.infra.setBaseUrl
import com.jssoftcom.comanda.model.Item
import com.jssoftcom.comanda.order.domain.data.remote.ResponseDefault
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.OrderListenerContract
import com.jssoftcom.comanda.order.itemselection.data.domain.remote.SubCategoryAdapter
import com.jssoftcom.comanda.ordersummary.OrderSummaryFragment
import com.jssoftcom.comanda.settings.parameters.ParametersPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sub_category_fragment.*
import retrofit2.Response

/**
 * Created by gustavon on 16/01/18.
 */
class SubCategoryFragment : Fragment(), ItemSelectionContract.View, OrderListenerContract {

     var categoriaId : String = ""

    lateinit var nome: String
    lateinit var nrMesa: String
    lateinit var nrCartao: String
    var selectedItems: ArrayList<Item> = arrayListOf()

    lateinit var presenter: ItemSelectionPresenter

    init {
        presenter = ItemSelectionPresenter()
        presenter.start()
    }

    override fun startPresenter() {
        presenter = ItemSelectionPresenter()
        presenter.start()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.sub_category_fragment, container, false)
    }

    fun getInfo() {
        val bundle = this.arguments
        if (bundle != null) {
            categoriaId = bundle.getString("id_categoria", "")
            if (arguments.getParcelableArrayList<Item>("SELECTED_ITEMS") != null){
                selectedItems = arguments.getParcelableArrayList<Item>("SELECTED_ITEMS")

                if (selectedItems.size > 0){
                    confirmItemSelection.visibility = View.VISIBLE
                }
            }

        }
    }

    fun getInfoTable() {
        val bundle = this.arguments
        if (bundle != null) {
            nome = bundle.getString(OpenTableFragment.NOME, "")
            nrMesa = bundle.getString(OpenTableFragment.NRMESA, "")
            nrCartao = bundle.getString(OpenTableFragment.NRCARTAO, "")
        }
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureConfirmItemSelection()
        getInfo()
        getInfoTable()

        // passa por todas categorias adicionando suas subcategorias
            ComandaApplication.database.subCategoriasDao().getSubCategoriaByCategoria(categoriaId)
            //ComandaApplication.database.subCategoriasDao().getAllSubCategorias()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { values ->
                        recyclerViewSubCategory.layoutManager = LinearLayoutManager(context)
                        recyclerViewSubCategory.adapter = SubCategoryAdapter(values as ArrayList<SubCategorias>, categoriaId, nome, nrMesa, nrCartao, selectedItems)
                    }

    }

    override fun success(settingsResponse: Response<ResponseDefault>) {
    }

    override fun erroMessage() {
    }

    private fun configureConfirmItemSelection() {
        confirmItemSelection.setOnClickListener {
            val bundle = Bundle()
            bundle.putParcelableArrayList("SELECTED_ITEMS", selectedItems)
            bundle.putString(OpenTableFragment.NRMESA, nrMesa)
            bundle.putString(OpenTableFragment.NRCARTAO, nrCartao)


            var itemSelectionFragment = OrderSummaryFragment()
            itemSelectionFragment.arguments = bundle

            activity.supportFragmentManager.beginTransaction()
                    .add(R.id.orderContainer, itemSelectionFragment, "item_selection")
                    .addToBackStack("item_selection").commit()

            for (item in selectedItems) {
                if (activity.application.setBaseUrl()) {
                    if (item.enviado.equals("n")){
                        item.enviado = "s"
                        var idGarcom = ParametersPreference.instance.getIdGarcom(this.activity, ParametersPreference.ID_GARCOM)

                        presenter.addProdutos(this, item.idProduto, nrMesa, nrCartao, nome, item.quantity.toString(), idGarcom, item.obsItem, item.idsOpcao)
                    }
                }
            }
        }
    }
}