package com.jssoftcom.comanda

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import java.util.HashMap

/**
 * Created by gustavon on 01/11/17.
 */
open class ComandaPreferences {

    var preferences: HashMap<Context, SharedPreferences> = HashMap()

    fun getSharedPreferences(context: Context): SharedPreferences {
        var pref = preferences.get(context)

        if (pref == null) {
            pref = PreferenceManager.getDefaultSharedPreferences(context)
            preferences.put(context, pref)
        }

        return pref!!
    }

    fun savePreference(context: Context, prefKey: String, newValue: String) {
        val mPreferences = getSharedPreferences(context)
        val editor = mPreferences.edit()
        editor.putString(prefKey, newValue)
        editor.apply()
    }

    fun getStringPreference(context: Context, prefKey: String, defaultValue: String): String {
        try {
            return getSharedPreferences(context).getString(prefKey, defaultValue)
        } catch (e: Exception) {
            return ""
        }

    }

    fun savePreference(context: Context, prefKey: String, newValue: Long?) {
        val mPreferences = getSharedPreferences(context)
        val editor = mPreferences.edit()
        editor.putLong(prefKey, newValue!!)
        editor.apply()
    }

    fun getLongPreference(context: Context, prefKey: String, defaultValue: Long?): Long? {
        return getSharedPreferences(context).getLong(prefKey, defaultValue!!)
    }

    fun savePreference(context: Context, prefKey: String, newValue: Boolean) {
        val mPreferences = getSharedPreferences(context)
        val editor = mPreferences.edit()
        editor.putBoolean(prefKey, newValue)
        editor.apply()
    }

    fun getBooleanPreference(context: Context, prefKey: String, defaultValue: Boolean): Boolean {
        try {
            return getSharedPreferences(context).getBoolean(prefKey, defaultValue)
        } catch (e: Exception) {
            return false
        }

    }
}