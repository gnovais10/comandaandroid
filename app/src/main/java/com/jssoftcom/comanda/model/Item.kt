package com.jssoftcom.comanda.model

import android.os.Parcel
import android.os.Parcelable
import com.jssoftcom.comanda.order.itemselection.ItemType
import com.jssoftcom.comanda.settings.categorias.domain.data.remote.SettingsResponse
import java.util.*
import kotlin.collections.ArrayList



data class Item(
        val id: String = UUID.randomUUID().toString(),
        var idProduto: String = "",
        var obsItem: String = "",
        var idsOpcao: String = "",
        var quantity: Int = 0,
        val description: String,
        val value: String = "",
        var enviado: String = "n",
        var selecionado: String = "n",
        var opcoes: ArrayList<SettingsResponse.Opcao> = arrayListOf(),
        val idCategoria: String = "") : Parcelable {

    companion object {
        @JvmField
        @Suppress("unused")
        final val CREATOR: Parcelable.Creator<Item> = object : Parcelable.Creator<Item> {
            override fun createFromParcel(source: Parcel): Item {
                return Item(source)
            }

            override fun newArray(size: Int): Array<Item?> {
                return arrayOfNulls(size)
            }
        }
    }

    protected constructor(parcelIn: Parcel) : this(
            idProduto = parcelIn.readString(),
            obsItem = parcelIn.readString(),
            idsOpcao = parcelIn.readString(),
            opcoes = parcelIn.createTypedArrayList(SettingsResponse.Opcao.CREATOR),
            idCategoria = parcelIn.readString(),
            quantity = parcelIn.readInt(),
            description = parcelIn.readString(),
            enviado = parcelIn.readString(),
            selecionado = parcelIn.readString(),
            value = parcelIn.readString()
    )

    override fun writeToParcel(dest: Parcel, flags: Int) {
        try {
            dest.writeString(idProduto)
            dest.writeString(obsItem)
            dest.writeString(idsOpcao)
            dest.writeString(idCategoria)
            dest.writeInt(quantity)
            dest.writeString(description)
            dest.writeString(enviado)
            dest.writeString(selecionado)
            dest.writeString(value)

            val arrayOpcoes: Array<SettingsResponse.Opcao> = opcoes!!.toTypedArray()
            dest.writeParcelableArray(arrayOpcoes, flags)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun describeContents() = 0

    override fun equals(other: Any?): Boolean {
        return this.description.equals((other as Item).description)
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}